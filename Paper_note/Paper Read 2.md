# Paper Read II

> Week from 20180820 to 20180826

## Efficient Visual Odometry and Mapping for UAV Using ARM-based Stereo Vision Pre-Processing System

> Changhong Fu, ICUAS 2015

### Abstract

Visual odometry and mapping methods can provide accurate environment information for UAV in GPS-denied environments. So a **light small-scale low-cost ARM-based stereo vision pre-processing system** was presented. The visual algorithm is evaluated with datasets from EuRoC Challenge III in terms of efficiency, accuracy and robustness. Finally, the new system is mounted on a real quadrotor UAV to test.

### Intro

In the past decades, the visual odometry and mapping algorithms have been developed fruitfully in the robotics. In the literature, monocular and stereo cameras are applied as two main vision tools.

#### Recent Works

For monocular camera, some robotics presented well work:

- M. Pizzoli et al [1] et al recently presented a real-time probabilistic monocular pose estimation method for 3D dense reconstruction.
- J. Engel et al [2] proposed a direct monocular Simultaneous Localization and Mapping (SLAM) algorithm for building consistent maps of the environments. These works have achieved promising results.

#### Monocular cameras

**Monocular cameras** cannot estimate the real absolute scale to surrounding environments, which may generate accumulated drifts.

So some others apply other other sensors-fused info to alleviate the problem:

- J. Zhang et al [3] adopted a 3D lidar device (i.e. a motor actuated rotated Hokuyo UTM-30LX) to enhance the visual odometry performance. 
- Our former work [4] fused an IMU sensor and a monocular camera onboard a UAV to successfully finish a see-and-avoid task.

the performances mainly lie on the accuracy of extra sensors, which will result in more expensive systems. And besides, some of the extra sensors are still too heavy to be carried.

#### Stereo cameras

**Stereo cameras** can effectively estimate information, thereby improving performances. However, stereo
cameras also have <u>two bottlenecks</u>: 

1. when the distance between the robot and the target is much larger than the baseline, the depth  estimation becomes inaccurate or simply invalid.
2. the features seen by only one side camera cannot be associated with the depth via real-time stereo matching

Some solution to this is that most works in this field have configured a large baseline to solve the first limitation in large-scale environments:

A. Geiger et al [5] set the baseline to more than a half meter for autonomy exploration, but this approach is not suitable for those typical UAVs.

Recently, different kinds of small-scale embedded system-based standalone stereo vision devices have been designed and applied for robots to process visual information, e.g. <u>VisLab 3DV system [6], Skybotix VI sensor [7] and DLR’s stereo device[8].</u>

[6] 3DV - An embedded, dense stereovision-based depth mapping system

[7]  A Synchronized Visual-Inertial Sensor System with FPGA Pre-Processing for Accurate Real-Time SLAM ICRA 2014

[8] Stereo vision and imu based real-time ego-motion and depth image computation on a handheld device ICRA 2013

### Referred Hardware, Software etc.

#### Hardware

- Asctec Pelican quadrotor platform http://www.asctec.de/en/

![AscTec-hummingbird](http://www.asctec.de/wp-content/uploads/2014/07/AscTec-hummingbird-research-drone-swarming-flight-dynamic-control-theory.jpg)

- UAS TECH http://www.uastech.com/wp/

![LinkQuad](http://www.uastech.com/LinkQuad_TL_smaller.jpg)

- Hokuyo UTM-30LX https://www.hokuyo-aut.jp/

https://www.hokuyo-usa.com/products/scanning-laser-rangefinders/utm-30lx

![UTM-30LX](https://www.hokuyo-usa.com/application/files/9214/7344/3925/thumbnail-UTM-30LX.jpg)

- VisLab 3DV http://vislab.it/products/3dv-e-system/

![3dv-e](http://vislab.it/wp-content/uploads/2014/07/fpga_euro.jpg)

- Skybotix VI sensor http://www.skybotix.com/ http://wiki.ros.org/vi_sensor/

![vi-sensor](http://wiki.ros.org/vi_sensor?action=AttachFile&do=get&target=vi-sensor-front.jpg)

![vi_sensor](http://www.skybotix.com/skybotix-light/wp-content/uploads/2014/10/DSC00850-corrected1-1024x343.jpg)

- ODROID http://www.hardkernel.com/

![c1-plus](http://www.hardkernel.com/main/_Files/prdt/2016/201606/G143452239825-1.jpg)

- IDS uEye industry cameras https://en.ids-imaging.com/home.html

![uEye](https://en.ids-imaging.com/store/media/catalog/category/IDS_Familienheader_IDS_USB3_LE_Camera_EN.jpg)

- Asus Xtion Pro Live https://www.asus.com/us/3D-Sensor/Xtion_PRO_LIVE/

http://xtionprolive.com/asus-3d-depth-camera

![xtion](http://xtionprolive.com/image/cache/catalog/sensors/33-300x300.jpg)

#### Software

- Camera Calibration Toolbox for Matlab

http://www.vision.caltech.edu/bouguetj/calib_doc/

![cctoolbox](http://www.vision.caltech.edu/bouguetj/calib_doc/gifs/pict_calib_mini2.gif)

- OctoMap http://octomap.github.io/ https://github.com/OctoMap/octomap http://wiki.ros.org/octomap

![octomap](https://i.loli.net/2018/08/20/5b7a3a507237b.png)



- Semi-Global Matching Approach

*Evaluation of a new coarse-to-fine strategy for fast semi-global stereo matching*

[*Semi-global matching: An alternative to lidar for DSM generation?*](http://www.isprs.org/proceedings/XXXVIII/part1/11/11_01_Paper_121.pdf)

- bucketing method

- FAST detector

*Machine Learning for High-speed Corner Detection* ECCV 2006

- BRIEF descriptor 

*BRIEF: Binary Robust Independent Elementary Features* ECCV 2010

- 2D and 3D Harris corner

 *A combined corner and edge detector*

- KLT tracker

*An Iterative Image Registration Technique with an Application to Stereo Vision* IJCAI1981

-  Bundle Adjustment 光束平差法

https://www.cnblogs.com/ironstark/p/5493030.html

https://blog.csdn.net/OptSolution/article/details/64442962

- EuRoC Challenge III project - European Robotics Challenges www.euroc-project.eu/

#### System Related

- ODROID

- eMMC 嵌入式多媒体储存卡（闪存）
- Ethernet with RJ-45 LAN Jack
- USB 2.0
- micro HDMI
- micro USB
- GPIO/UART/I2C ports
- IDS uEye industry cameras (type: UI-1221LE-C-HQ) 

- lenses (i.e. Lensagon BM2820) https://www.lensation.de/