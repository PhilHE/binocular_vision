

## Convolutional Neural Network-based Place Recognition

---

### 1 Introduction 

Since their introduction in the early 1990s, Convolutional Neural Networks (CNNs) have been used to achieve excellent performance on a variety of tasks such as handwriting recognition and face detection. More recently, supervised deep convolutional neural networks have been shown to deliver high level performance on more challenging classification tasks [Krizhevsky, et al., 2012]. The key supporting factors behind these impressive results are their ability to learn tens of millions of parameters using large amounts of labelled data. Once trained in this way, CNNs have been shown to learn discriminative and human- interpretable feature representations [Zeiler and Fergus, 2013]. Most impressively, these approaches are capable of producing state of the art performance on tasks that the model was not explicitly trained for [Donahue, et al., 2013], including object recognition on the Caltech- 101 dataset [Fei-Fei, et al., 2007], subcategory recognition on the Caltech-USCD birds dataset [Welinder, et al., 2010], scene recognition on the SUN- 397 dataset [Xiao, et al., 2010] and object detection on the PASCAL VOC dataset [Girshick, et al., 2013]. This good generalization in performance on new tasks and datasets indicates that CNNs may provide a general and universal visual feature learning framework applicable to all tasks. Encouraged by these positive results, in this paper we develop a place recognition framework centered around features from pre-trained CNNs as illustrated in Figure 1.

自20世纪90年代初推出以来，卷积神经网络（CNN）已经被用于诸如手写字体识别和人脸检测等各种任务中，并取得出色的表现。最近，监督的深度卷积神经网络已经显示出在更具挑战性的分类任务上提供高水平的性[Krizhevsky，et al 2012]。这些令人印象深刻的结果背后的关键支撑因素是他们使用大量标签数据学习数千万个参数的能力。一旦以这种方式进行了训练，CNN已被证明，能够学习辨别性和人类可解释的特征表示[Zeiler and Fergus，2013]。最令人印象深刻的是，这些方法能够在模型未明确训练的任务上产生最先进的表现[Donahue等人，2013]，包括在Caltech-101数据集上的对象识别[Fei-Fei等，2007]，Caltech-USCD鸟类数据集上的子类识别[Welinder等，2010]，SUN-397数据集上[的场景识别Xiao等，2010]和PASCAL VOC数据集的对象检测[Girshick等，2013]。这个在新的任务和数据集上很好的泛化表现，表明CNN可以提供适用于所有任务的通用的视觉特征学习框架。在这些积极成果的鼓舞下，本文开发了一个**基于预先训练CNN的特征的场景识别框架**，如图1所示。

![1](https://img-blog.csdn.net/20170605142736911)

Place recognition can be considered as an image retrieval task which consists of determining a match between the current scene and a previously visited location. State-of-the-art visual SLAM algorithms such as FAB-MAP [Cummins and Newman, 2008] match the appearance of the current scene to a past place by converting the image into bag-of-words representations [Angeli, et al., 2008] built on local features such as SIFT or SURF. However, recent evidence [Krizhevsky, et al., 2012] suggests that features extracted from CNNs trained on very large datasets significantly outperform SIFT features on classification tasks. Donahue [Donahue, et al., 2013] shows that using mid-level features from CNN models trained on the ImageNet database can more efficiently remove dataset bias in some of the domain adaption studies than a bag of words approach.

场景识别可以被认为是图像检索任务，其包括确定当前场景和先前访问的位置之间的匹配。 最先进的视觉SLAM算法，如FAB-MAP [Cummins and Newman, 2008] ，通过将图像转换为词袋表示，将当前场景的外观与过去的地方相匹配[Angeli，et al，2008]建立在SIFT或SURF等本地功能的基础上。 然而，最近的证据 [Krizhevsky，et al，2012]表明，从非常大的数据集上训练的CNN中提取的特征在分类任务上明显优于SIFT特征。 Donahue [Donahue，et al，2013]表明，使用在ImageNet数据库上训练的CNN模型的中级特征可以更有效地消除某些领域适应性研究中的数据集偏差，相比较与词袋方法。

In this paper we investigate whether the advantages of deep learning in other recognition tasks carries over to place recognition. We present a deep learning-based place recognition algorithm that compares the response of feature layers from a CNN trained on ImageNet [Deng, et al., 2009] and methods for filtering the subsequent place recognition hypotheses. We conduct two experiments, one on a 70 km benchmark place recognition dataset, and one on a viewpoint varying dataset, providing both quantitative comparison to two state of the art place recognition algorithms and analysis of the utility of different layers within the network for viewpoint invariance.

在本文中，我们研究深度学习在其他识别任务中的优势是否会带来认知。 我们提出了一种基于深度学习的位置识别算法，该算法比较来自在ImageNet上训练的CNN的特征层的响应[Deng，et al。，2009]和用于过滤后续位置识别假设的方法。 我们进行了两次实验，一次是在70 km位置识别数据集上，另一次是在视点变化数据集上，提供了两种现有技术位置识别算法的定量比较，并分析了网络中不同层的效用，用于viewpoint invariance.

The paper proceeds as follows. Section 2 provides an overview of feature-based place recognition techniques and convolutional neural networks. In Section 3 we describe the components of the deep learning-based place recognition system. The experiments are described in Section 4, with results presented in Section 5. Finally we conclude the paper in Section 6 and discuss ongoing and future work.

论文陈述如下。 第2节概述了基于特征的位置识别技术和卷积神经网络。 在第3节中，我们描述了基于深度学习的场所识别系统的组件。 实验在第4节中描述，结果在第5节中介绍。最后，我们在第6节中总结了论文并讨论了正在进行的和未来的工作

### 2 Related Work

In this section, we briefly review feature-based representations for place recognition and the use of convolutional neural networks for various visual classification tasks.

在本节中，我们将简要回顾基于特征的位置识别表示以及卷积神经网络在各种视觉分类任务中的使用。

#### 2.1 Vision Representation for Place Recognition 

Visual sensors are increasingly becoming the predominant sensor modality for place recognition due to their low cost, low power requirements, small footprint and rich information content. There has been extensive research on how to best represent and match images of places. 

视觉传感器由于其低成本，低功率要求，小占地面积和丰富的信息内容而越来越成为用于位置识别的主要传感器模态。 关于如何最好地表现和匹配场所图像已经进行了广泛的研究。

Several authors have described approaches that apply global feature techniques to process incoming sensor information. In [Murillo and Kosecka., 2009], the authors propose a gist feature-based place recognition system using panoramic images for urban environments. Histograms of image gray values or texture is also a widely used feature in place recognition systems [Ulrich and Nourbakhsh, 2000, Blaer and Allen, 2002] due to its compact representation rotation invariance. However, global features are computed from the entire image, rendering them unsuitable to effects such as partial occlusion, lighting change or perspective transformation [Deselaers, et al., 2008]. 

一些作者描述了应用全局特征技术来处理传入传感器信息的方法。在[Murillo and Kosecka。，2009]中，作者提出了一个基于特征的基于特征的地方识别系统，该系统使用全景图像进行城市环境。由于其紧凑的表示旋转不变性，图像灰度值或纹理的直方图也是位置识别系统中广泛使用的特征[Ulrich和Nourbakhsh，2000，Blaer和Allen，2002]。然而，全局特征是从整个图像计算出来的，使得它们不适合诸如部分遮挡，光照变化或透视变换之类的效果[Deselaers，et al，2008]。

Local features are less sensitive to these external factors and have been widely used in appearance-based loop closure detection, SIFT [Lowe, 1999] and SURF [Herbert Bay, et al., 2008] being two widespread examples. State-of-the-art SLAM systems such as FAB- MAP [Cummins and Newman, 2008] further **represent appearance data using sets of local features, converting images into “bag-of-words”,** which enables efficient retrieval. Other feature-less representations have also been proposed. SeqSLAM [Milford and Wyeth, 2012] directly uses pixel values to match image sequences and perform place recognition across extreme perceptual changes. However, it is rapidly becoming apparent in other recognition tasks that hand-crafted features are being outperformed by learnt features, prompting the question of whether we can learn better features automatically?

局部特征对这些外部因素不太敏感，并且已广泛用于基于外观的环闭合检测，SIFT [Lowe，1999]和SURF [Herbert Bay，et al，2008]是两个广泛的例子。最先进的SLAM系统，如FAB-MAP [Cummins和Newman，2008]，使用多组局部特征进一步表示外观数据，将图像转换为“词袋”，从而实现高效检索。还提出了其他无特征表示。 SeqSLAM [Milford和Wyeth，2012]直接使用像素值来匹配图像序列，并在极端感知变化中执行位置识别。但是，在其他识别任务中，手工制作的功能在学习功能方面表现得越来越明显，这就引发了我们是否可以自动学习更好的功能的问题。

#### 2.2 Convolutional Neural Networks 卷积神经网络

Convolutional neural networks are multi-layer supervised networks which can learn features automatically from datasets (Figure 3). For the last few years, CNNs have achieved state-of-the-art performance in almost all important classification tasks [Krizhevsky, et al., 2012, Donahue, et al., 2013, Sharif Razavian, et al., 2014]. Their primary disadvantage is that they require very large amounts of training data. However, recent studies have shown that state of the art performance can be achieved with networks trained using “generic” data, raising the possibility of developing a place recognition system based on features learnt from datasets with a classification focus. A similar approach has already achieved excellent performance on various visual tasks, such as object recognition [Fei-Fei, et al., 2007]; subcategory recognition [Welinder, et al., 2010]; scene recognition [Xiao, et al., 2010] and detection [Girshick, et al., 2013].

卷积神经网络是多层监督网络，可以从数据集中自动学习特征（图3）。在过去几年中，CNN在几乎所有重要的分类任务中都取得了最先进的表现[Krizhevsky等，2012，Donahue等，2013，Sharif Razavian等，2014]。它们的主要缺点是它们需要非常大量的训练数据。然而，最近的研究表明，使用“通用”数据训练的网络可以实现最先进的性能，从而提高了基于从具有分类焦点的数据集学习的特征开发位置识别系统的可能性。类似的方法已经在各种视觉任务上取得了优异的性能，例如物体识别[Fei-Fei，et al。，2007];子类别识别[Welinder，et al。，2010];场景识别[Xiao，et al。，2010]和检测[Girshick，et al。，2013]。

One research area separate but relevant to the place recognition problem is the task of image retrieval where a query image is present to a database to search for those images containing the same objects or scenes. In [Babenko, et al., 2014], mid-level features from CNNs are evaluated for the image retrieval application and achieve performance comparable to others using state- of-the-art features. **Interestingly, the best performance is obtained using mid-network features rather than those learnt at the final layers.** 

与位置识别问题分离但相关的一个研究区域是图像检索的任务，其中查询图像存在于数据库以搜索包含相同对象或场景的那些图像。在[Babenko，et al。，2014]中，来自CNN的中级特征被评估用于图像检索应用并且使用最先进的特征实现与其他特征相当的性能。有趣的是，使用中间网络功能而不是在最后一层学习的功能可以获得最佳性能。

**Place recognition is essentially a task of image similarity matching.** In [Fischer, et al., 2014], features from various layers of CNNs are evaluated and compared with SIFT descriptors on a descriptor matching benchmark. The benchmark results demonstrate that deep features from different layers of CNNs consistently perform better than SIFT on descriptor matching; indicating that SIFT or SURF may not be the preferred descriptors for matching tasks anymore. **Our paper is thus inspired by the excellent performance of CNNs on image classification and the evidence of their feasibility in feature matching.**

场所识别本质上是图像相似性匹配的任务。在[Fischer，et al。，2014]中，评估来自各层CNN的特征，并与描述符匹配基准上的SIFT描述符进行比较。基准测试结果表明，来自不同CNN层的深层特征在描述符匹配上始终比SIFT表现更好;表明SIFT或SURF可能不再是匹配任务的首选描述符。因此，我们的论文受到了CNN在图像分类方面的出色表现以及它们在特征匹配方面的可行性的启发。

### 3 Approach and Methodology

In this section we describe the two key components of our approach: **feature extraction and spatio-temporal filtering of place match hypotheses output by comparison of feature responses**. The schematic illustration of the method procedure is shown in Figure 2:

在本节中，我们描述了我们方法的两个关键组成部分：**特征提取和场景匹配假设的时空过滤波**，通过特征响应的比较输出。 方法过程的示意图如图2所示：

![cnn_place](./cnn_place.png)

We use a pretrained network called Overfeat [Sermanet, et al., 2013] which was originally proposed for the ImageNet Large Scale Visual Recognition Challenge 2013 (ILSVRC2013). The Overfeat network is trained on the ImageNet 2012 dataset, which consists of 1.2 million images and 1000 classes. 

The network comprises five convolution stages and three fully connected stages (Figure 3). 

Each of the bottom two convolution stages consists of a convolution layer, a max pooling layer and a rectification (ReLU) non- linearity layer. 

The third and fourth stages consist of a convolution layer, a zero-padding layer and a ReLU non-linear layer. 

The fifth stage contains a convolution layer, a zero-padding layer, a ReLU layer and a max- pooling layer. 

the sixth and seventh stages contain one fully-connected layer and one ReLU layer

the eighth contains only the fully-connected output layer. 

In total, there are 21 layers.

我们使用名为Overfeat的预训练网络[Sermanet，et al。，2013]，该网络最初是为2013年ImageNet大规模视觉识别挑战赛（ILSVRC2013）提出的。 Overfeat网络在ImageNet 2012数据集上进行训练，该数据集包含120万个图像和1000个类。 该网络包括五个卷积级和三个完全连接的级（图3）。 底部两个卷积级中的每一个由卷积层，最大池化层和整流（ReLU）非线性层组成。 第三和第四级包括卷积层，零填充层和ReLU非线性层。 第五级包含卷积层，零填充层，ReLU层和最大池层。 最后，第六和第七级包含一个完全连接的层和一个ReLU层，而第八级仅包含完全连接的输出层。 总共有21层。

When an image I is input into the network, it produces a sequence of layered activations. We use $L_k(l), k = 1, … ,21$ to denote the corresponding output of the $k^{th}$ layer given input image I. Each of these vectors is a deep learnt representation of the image I; place recognition is performed by comparing these feature vector responses to different images. The network is capable of processing images of any size equal to or greater than 231 × 231 pixels, consequently all experiments described here used images resized to 256 × 256 pixels.
Figure











参考文件:

- overfeat

OverFeat: Integrated Recognition, Localization and Detection using Convolutional Networks

https://blog.csdn.net/LeeWanzhi/article/details/79504165

https://blog.csdn.net/buwan86658/article/details/78054081

https://blog.csdn.net/langb2014/article/details/52334490

https://blog.csdn.net/yimingsilence/article/details/53995721





