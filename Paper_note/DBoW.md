### DBoW

---

> 正负检索需要重点掌握

### Explanation

- [Bag-of-words model in computer vision](https://en.wikipedia.org/wiki/Bag-of-words_model_in_computer_vision)

Overview

To achieve this, it usually includes following three steps: feature detection, feature description, and codebook generation.(Fei-Fei Li; Perona, P. (2005). "A Bayesian Hierarchical Model for Learning Natural Scene Categories". CVPR05)

1. feature detection
2. feature description
3. codebook generation

The final step for the BoW model is to convert vector-represented patches to "codewords" (analogous to words in text documents). One simple method is performing k-means clustering over all the vectors.

Strengths & Drawbacks

### Chinese Analysis

[BoW（词袋）模型详细介绍 - SwiftlyFly - CSDN博客 ](https://blog.csdn.net/u010213393/article/details/40987945)

[BoW算法及DBoW2库简介](https://www.cnblogs.com/mafuqiang/p/7111424.html)

[创建DBow离线词典用于ORB SLAM2](https://www.jianshu.com/p/cfcdf12a3bb6)

### Codes

- [dorian3d/DBoW2: Enhanced hierarchical bag-of-word library for C++ ](https://github.com/dorian3d/DBoW2)
- [rmsalinas/DBow3: Improved version of DBow2](https://github.com/rmsalinas/DBow3)
- [raulmur/ORB_SLAM2: Real-Time SLAM for Monocular, Stereo and RGB-D Cameras, with Loop Detection and Relocalization Capabilities ](https://github.com/raulmur/ORB_SLAM2)



[【泡泡机器人原创专栏】DBoW3 视觉词袋模型、视觉字典和图像数据库分析](https://mp.weixin.qq.com/s?__biz=MzI5MTM1MTQwMw==&mid=2247487947&idx=1&sn=a161d5ba005adabbfef23ee823c3f34b&chksm=ec10afcfdb6726d9460e3992357b93a50fb622a805c785a9322d7cafb6f8d7d0b02494206fbd&mpshare=1&scene=1&srcid=0120tujPrzQBRJvOMRlHZuAr&pass_ticket=DyCv5iDYNGzqu%2FG5eHjGG4I5gZSFV%2B4a6kb08nDUOcc%3D#rd)

[词袋模型一些理解](

https://blog.csdn.net/icameling/article/details/80923777?utm_source=blogxgwz6	
)