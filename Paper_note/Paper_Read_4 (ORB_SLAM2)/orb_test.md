ORB_Test

---

- [ORB_SLAM2安装与运行](https://blog.csdn.net/myarrow/article/details/53045405)

Monocular 实例
​      1）TUM 数据集
从http://vision.in.tum.de/data/datasets/rgbd-dataset/download下载并解压一个序列，如：rgbd_dataset_freiburg1_desk2.tgz

执行下面的命令（把TUMX.yaml 修改为TUM1.yaml < freiburg1序列>,TUM2.yaml < freiburg2序列> or TUM3.yaml < freiburg3序列> ）

/home/philhe/Desktop/VO_src/dataset/tum/rgbd_dataset_freiburg1_rpy

在`ORB_SLAM2`文件夹中

```bash
$ ./Examples/Monocular/mono_tum Vocabulary/ORBvoc.txt ./Examples/Monocular/TUMX.yaml PATH_TO_SEQUENCE_FOLDER 

// for example

./Examples/Monocular/mono_tum Vocabulary/ORBvoc.txt ./Examples/Monocular/TUM1.yaml /home/philhe/Desktop/VO_src/dataset/tum/rgbd_dataset_freiburg1_rpy

./Examples/Monocular/mono_tum Vocabulary/ORBvoc.txt ./Examples/Monocular/TUM1.yaml /home/philhe/Desktop/VO_src/dataset/tum/rgbd_dataset_freiburg1_xyz

./Examples/Monocular/mono_tum Vocabulary/ORBvoc.txt ./Examples/Monocular/TUM2.yaml /home/philhe/Desktop/VO_src/dataset/tum/rgbd_dataset_freiburg2_xyz
```


​     2） KITTI 数据集

从 http://www.cvlibs.net/datasets/kitti/eval_odometry.php下载数据集（灰度图像）， 把KITTIX.yaml  修改为 KITTI00-02.yaml, KITTI03.yaml or KITTI04-12.yaml，这些*xx.yaml各自对应于序列 0 to 2, 3, and 4 to 12. Change PATH_TO_DATASET_FOLDER to the uncompressed dataset folder. Change SEQUENCE_NUMBER to 00, 01, 02,.., 11.如有不明白的地方，看一下代码：

```
./Examples/Monocular/mono_kitti Vocabulary/ORBvoc.txt Examples/Monocular/KITTIX.yaml PATH_TO_DATASET_FOLDER/dataset/sequences/SEQUENCE_NUMBER
```

- [ORB-SLAM2 mono-kitti 系统分析图](https://blog.csdn.net/u013945158/article/details/77823951)
- [从编译运行orbslam2说起](https://www.cnblogs.com/huaxiaforming/p/6691610.html)

