用电脑自带的摄像头跑orb_slam2 https://zhuanlan.zhihu.com/p/29629824

ROS kinetic 下单目摄像机的标定 https://blog.csdn.net/xu1129005165/article/details/52948992

第三步,编译orb_slam2及相关库。根据orb_slam列出来的依赖项都需要安装。

安装**Pangolin**

先安装一些依赖项

```text
sudo apt-get install libglew-dev  
sudo apt-get install cmake  
sudo apt-get install libboost-dev libboost-thread-dev libboost-filesystem-dev
```

然后安装Pangolin

```text
git clone https://github.com/stevenlovegrove/Pangolin.git  
cd Pangolin  
mkdir build  
cd build  
cmake -DCPP11_NO_BOOST=1 ..  
make -j 
```

安装**Eigen3**

```text
sudo apt-get install libeigen3-dev
```

Eigen与其他库不同，它是一个由头文件搭建起来的库，Eigen头文件的默认安装位置在“/usr/include/eigen3/”中。我们在使用时，只需引入Eigen头文件，不需要链接它的库文件，在CMakeLists.txt里添加Eigen头文件的目录。

**下载orb_slam2**

把orb_slam２安装在我们最开始建立的catkin_ws空间里面。

```text
cd catkin_ws／src
git clone https://github.com/raulmur/ORB_SLAM2.git ORB_SLAM2
```

接下来，我们就需要安装orb_slam的一些依赖项。先安装g2o，安装g2o之前，需要先安装一下依赖项。这里最后一个需要注意一下，具体可以看我的博客[slam安装中libcholmod-dev文件找不到的解决办法 - hitfangyu的博客 - CSDN博客](https://link.zhihu.com/?target=http%3A//blog.csdn.net/hitfangyu/article/details/72665455)

```text
sudo apt-get install libcholmod-dev(注：最后一个依赖项需要table键来填充名称)
sudo apt-get install libqt4-dev qt4-qmake libqglviewer-dev 
sudo apt-get install libsuitesparse-dev
sudo apt-get install libcxsparse3.1.4
```

然后安装orb_slam提供的g2o工具

```text
cd catkin_ws/src/ORB_SLAM2/Thirdparty/g2o/
mkdir build && cd build
cmake ..
make
```

接下来编译*DBoW2*

```text
cd catkin_ws/src/ORB_SLAM2/Thirdparty/DBoW2
mkdir build && cd build
cmake ..
make
```

一切准备就绪，接下来编译orb_slam2

```text
cd catkin_ws/src/ORB_SLAM2
mkdir build 
cd build 
cmake ..
make 
```

---

ORBSLAM2 make时间报错

```
[  0%] Built target rospack_genmsg_libexe
[  0%] Built target rosbuild_precompile
make[2]: *** No rule to make target '../../../../lib/libORB_SLAM2.so', needed by '../RGBD'.  Stop.
CMakeFiles/Makefile2:67: recipe for target 'CMakeFiles/RGBD.dir/all' failed
make[1]: *** [CMakeFiles/RGBD.dir/all] Error 2
Makefile:127: recipe for target 'all' failed
make: *** [all] Error 2
```

https://blog.csdn.net/cdlwhm1217096231/article/details/79177189/

https://github.com/raulmur/ORB_SLAM2/pull/507/commits/5a5cc2aba95bcd40dfb41a7b084ad6c8ef60fa64

还是报错,官方输入

```
export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:/home/philhe/catkin_ws/src/ORB_SLAM2/Examples/ROS
```

或者

```
export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:/home/philhe/Desktop/orbslam2/catkin_ws/src/ORB_SLAM2/Examples/ROS
```

仍然报错

```
make[2]: *** No rule to make target '../../../../lib/libORB_SLAM2.so', needed by '../Stereo'.  Stop.
CMakeFiles/Makefile2:104: recipe for target 'CMakeFiles/Stereo.dir/all' failed
make[1]: *** [CMakeFiles/Stereo.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs....
make[2]: *** No rule to make target '../../../../lib/libORB_SLAM2.so', needed by '../RGBD'.  Stop.
CMakeFiles/Makefile2:67: recipe for target 'CMakeFiles/RGBD.dir/all' failed
make[1]: *** [CMakeFiles/RGBD.dir/all] Error 2
make[2]: *** No rule to make target '../../../../lib/libORB_SLAM2.so', needed by '../Mono'.  Stop.
CMakeFiles/Makefile2:718: recipe for target 'CMakeFiles/Mono.dir/all' failed
make[1]: *** [CMakeFiles/Mono.dir/all] Error 2
make[2]: *** No rule to make target '../../../../lib/libORB_SLAM2.so', needed by '../MonoAR'.  Stop.
CMakeFiles/Makefile2:820: recipe for target 'CMakeFiles/MonoAR.dir/all' failed
make[1]: *** [CMakeFiles/MonoAR.dir/all] Error 2
Makefile:127: recipe for target 'all' failed
make: *** [all] Error 2
```

在主文件夹报错

```
CMakeFiles/ORB_SLAM2.dir/build.make:62: recipe for target 'CMakeFiles/ORB_SLAM2.dir/src/System.cc.o' failed
make[2]: *** [CMakeFiles/ORB_SLAM2.dir/src/System.cc.o] Error 1
CMakeFiles/Makefile2:178: recipe for target 'CMakeFiles/ORB_SLAM2.dir/all' failed
make[1]: *** [CMakeFiles/ORB_SLAM2.dir/all] Error 2
Makefile:83: recipe for target 'all' failed
make: *** [all] Error 2
```

http://www.bubuko.com/infodetail-2038049.html

解决方案如下:

```
3）usleep未定义：
错误信息：

/home/melanie/source/SmartCar/ORM_SLAM2/ORB_SLAM2/src/Viewer.cc:159:28: error: ‘usleep’ was not declared in this scope
usleep(3000);
^
CMakeFiles/ORB_SLAM2.dir/build.make:494: recipe for target ‘CMakeFiles/ORB_SLAM2.dir/src/Viewer.cc.o‘ failed
make[2]: *** [CMakeFiles/ORB_SLAM2.dir/src/Viewer.cc.o] Error 1
CMakeFiles/Makefile2:178: recipe for target ‘CMakeFiles/ORB_SLAM2.dir/all‘ failed
make[1]: *** [CMakeFiles/ORB_SLAM2.dir/all] Error 2
Makefile:83: recipe for target ‘all‘ failed
make: *** [all] Error 2

解决方案：

在source文件的开头增加include
#include <unistd.h>

需要增加unistd.h的文件有：
Examples/Monocular/mono_euroc.cc
Examples/Monocular/mono_kitti.cc
Examples/Monocular/mono_tum.cc
Examples/RGB-D/rgbd_tum.cc
Examples/Stereo/stereo_euroc.cc
Examples/Stereo/stereo_kitti.cc
src/LocalMapping.cc
src/LoopClosing.cc
src/System.cc
src/Tracking.cc
src/Viewer.cc
```

[linux c之#include <unistd.h> 总结](https://blog.csdn.net/u011068702/article/details/54286300)

---

修改asus.xml,相关方程

```
> cameraParams.IntrinsicMatrix
ans =
 815.3634         0         0
        0  816.1348         0
 339.1092  234.1194    1.0000

>> cameraParams.RadialDistortion
ans =
  -0.2969    1.6441

>> cameraParams.TangentialDistortion
ans =
    0     0
```

相关参数

我的位置

```
/home/philhe/catkin_ws/src/ORB_SLAM2/Examples/ROS/ORB_SLAM2
```

---

开始运行

１．首先打开ros的终端

```text
roscore
```

2.打开摄像头

```text
roslaunch usb_cam usb_cam-test.launch
```

3.运行orb_slam

```text
rosrun ORB_SLAM2 Mono /home/philhe/catkin_ws/src/ORB_SLAM2/Vocabulary/ORBvoc.txt /home/philhe/catkin_ws/src/ORB_SLAM2/Examples/ROS/ORB_SLAM2/Asus_he.yaml
```

这里的电脑名字，请替换为你自己的。

这里说一下，如果你装的是opencv3.1或者3.3的话，应该都会报错，无法启动，错误类型是opencvError，提示你有地方要求是2.4.8。我在这里找了很久，发现是因为ros把openv版本写死了，他只认这个版本，所以没办法，我只能改了。我在官网也没找到2.4.8的，后来在github上面找到了,这里把地址贴出来：[opencv2.4.8](https://link.zhihu.com/?target=https%3A//github.com/bigjun/opencv-2.4.8).

４.打开rqt_graph

```text
rqt_graph 
```

---

运行时WARNING

```
[ INFO] [1539416522.990419919]: using default calibration URL
[ INFO] [1539416522.990488063]: camera calibration URL: file:///home/philhe/.ros/camera_info/head_camera.yaml
[ INFO] [1539416522.990544110]: Unable to open camera calibration file [/home/philhe/.ros/camera_info/head_camera.yaml]
[ WARN] [1539416522.990569907]: Camera calibration file /home/philhe/.ros/camera_info/head_camera.yaml not found.
[ INFO] [1539416522.990594642]: Starting 'head_camera' (/dev/video0) at 640x480 via mmap (yuyv) at 30 FPS
[ INFO] [1539416523.111666809]: Using transport "raw"
[ WARN] [1539416523.114066268]: sh: 1: v4l2-ctl: not found

[ WARN] [1539416523.116593993]: sh: 1: v4l2-ctl: not found
[ WARN] [1539416523.114066268]: sh: 1: v4l2-ctl: not found
```

https://blog.csdn.net/xu1129005165/article/details/52948992

4 警告: 
`unknown control 'focus_auto' head_camera.yaml not found` 
可以忽略.