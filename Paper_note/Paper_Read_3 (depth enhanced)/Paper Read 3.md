# Paper Read III

> Week from 20180827 to 20180923

## Real-time Depth Enhanced Monocular Odometry

> Ji Zhang, Michael Kaess, and Sanjiv Singh, IROS 2014

### 0. Abstract

- **Visual odometry** can be augmented by **depth data** got from sensors
- Depth information can be limited by the sensors
- propose a method to utilize the depth when data is sparsely available

- method core is a **bundle adjustment** that refines the motion estimates in parallel by processing a sequence of images, in a batch optimization.
- evaluated in three sensor setups, one using an RGB-D camera, and two using combinations of a camera and a 3D lidar

- In addition, the method utilizes depth by triangulation from the previously estimated motion, and salient visual features for which depth is unavailable
- bundle adjustment



### I. Intro

- Visual odometry is the process of egomotion estimation given a sequence of camera imagery.

**monocular**

- monocular imagery is insufficient to compute the egomotion because that 
  motion along the camera optical axis can cause little motion of visual features and it's hard to solve estimation problem.
- single camera's estimation result may be dependant on other info (data from other sensors) or condtions (constraints) involved

**multiple cameras**

- cost–reduced effective field of view and a limitation on the range
- There is a gap between small baseline and uncertain depth features

**This paper**

- a method that can effectively utilize depth information along with the imagery, even the depth is only sparsely provided
- maintains and registers a depth map using the estimated motion of the camera
- **?** Salient visual features for unavailable depth area are also used, which themselves provide constraints in solving the problem.
- contains a bundle adjustment which refines the motion estimates in parallel by processing a sequence of images, in a batch optimization
- **not limited to RGB-D cameras and can be adapted to various types of cameras as long as depth information can be acquired**



### II. Related Work

- Vision based methods are now common for motion estimation

**stereo vision systems** 

- often used to solve 6DOF camera motion
- track and match visual features between image frames
- depth information of the features are retrieved by triangular geometry with the camera baseline reference
  - Paz et al. get depth info recovered for features to solve scale of the translation estimation in estimating stereo hand-hold cameras motion
  - Konolige, at al use a method integrated with an IMU to realize stereo visual odometry recovering motion from bundle adjustment
- RGB-D cameras has drawn great attention in the research of VO
  - Huang et al. [20] use tracked visual features with known RGB-D camera depth info to compute the motion estimates
  - **?** Henry et al. [21] integrate visual features with the Iterative Closest Point (ICP) method. The motion estimation employs a joint optimization by minimizing combined 2D and 3D feature matching errors.
  - Popular dense tracking [23], [24]. method minimizes the photometric error using a dense 3D model of the environment.
- depth information can only be limitedly available when using these methods.

- Few RGB-D visual odometry methods are able to **handle insufficiently** provided depth information.
  - Hu et al., a heuristic switch is used to choose between an RGB-D and a monocular visual odometry method [25]. In contrast to these methods [20], [21], [23]–[25], we propose a single method to handle sparse depth information, by combining both features with and without depth. The method is compared to [20], [23] experimentally, and robust estimation results are shown. Further, since the method maintains and registers a depth map, it can use depth information from different types of sensors. The method is currently tested with depth from RGB-D cameras and lidars, but is supposed to work with depth from stereo cameras also.  



### III. Notations &Task Desciption

**Main Problem**: Estimate the camera motion using monocular images
<u>with assistance of depth information</u>

- a pinhole camera with calibrated parameters and removed lens distortion.

we use right superscript k, k ∈ $Z^+$ to indicate image frames.

- {C} is a 3D coordinate system

C's origin at the **camera optical center**. 

1. **x-axis** points to the left
2. **y-axis** points upward
3. **z-axis** points forward coinciding with the camera principal axis(depth)

#### 3.1 features with and without depth

$I$ be a set of feature points, $i$ as a feature point with feature

$i$ in {$X^k$} are denoted as ${X_i}^k = \{ {x_i}^k, {y_i}^k, {z_i}^k \}^T$

unknown feature's z is normalized as 1, ${\overline{X}_i}^k$ is the point projected onto the plane.

#### 3.2 Following Problems

Given a sequence of image frames $k$, $k ∈ Z^+$ , and features, $I$, compute the motion of the camera between each two consecutive frames, k and k − 1, using ${X_i}^k$, if the
depth is available, and ${\overline{X}_i}^k$ , if the depth is unknown, $i ∈ I$.



### IV. SYSTEM OVERVIEW

#### 4.1 Sensor Hardware

- three different sensor systems
- two acquire data, and one uses KITTI benchmark datasets configuration
- a) Xtion Pro Live RGB-D camera; b) Hokuyo UTM-30LX laser scanner

#### 4.2 Software Overview

+ **Visual features** are tracked by the <u>feature tracking block</u>
+ **Depth Info**(Depth images from RGB-D or point clouds from lidars) is registered by the <u>depth map registration block</u>, associate with visual features
+ the **features as the input**, and its output is refined by the bundle adjustment block using sequences of images is in <u>frame to frame motion estimation block</u>
+ <u>transform integration block</u> combines the **high frequency frame to frame motion with the low frequency refined motion**

![Software Arch](.\Software Arch.png)



### V. FRAME TO FRAME MOTION ESTIMATION

#### 5.1 Mathematical Derivation

##### Model begin with frame to frame motion estimation

camera motion as rigid body transformation: 

${X_i}^k = R {X_i}^{k-1} + T$ **(1)**

(3x1) = (3x3) (3x1) + (3x1)

第i个特征在第$C^k$投射空间中的坐标是$C^{k-1}$坐标经过刚体变换后的结构。

- known depth

normalized正则化之后，重写上述公式：

${z_i}^k {\overline{X}_i}^k = R {X_i}^{k-1} + T$ **(2)**

上述最终结果得到(3x1)矩阵，因此分x,y,z分别写出方程；等同于解耦，将z行带入直接对${X_i}^k$中前两行进行操作，得到；

$ (R_1 - {\overline{x}_1}^k R_3){X_i}^{k-1} + T_1 - {\overline{x}_i}^k T_3 = 0 $ **(3)(4)**

- unknown depth

重写**(1)**，同样对$ {X_i}^{k-1} $采用正交化表现，可以得出下式：

$ {z_i}^k {\overline{X}_i}^k = {z_i}^{k-1} R {\overline{X}_i}^{k-1} + T $ **(5)**

同样一一写出方程每一行各自化简可得下述方程：

 **(6)**

##### Solve the motion using both types of features

旋转角矩阵定义：Define $\theta = [\theta_x, \theta_y, \theta_z]^T$

根据[Rodrigues formula](https://en.wikipedia.org/wiki/Rodrigues%27_formula)方程结合旋转角可以求出旋转矩阵R

$R = ...$ **(7)**

将生成的方程(7)导入带有深度信息的(3)(4)，导入不带深度信息的(6)

主要未知变量为$\theta$和$T$，假设有m个已知深度和n个未知深度信息来求解方程，得到一个nonlinear方程：

$f([T; \theta]) = \Epsilon$  **(8)**

其中$ \Epsilon $是(2m+n)x1残差向量，之后计算f的雅可比矩阵

$ J = \dfrac{\partial f}{\partial [T; \theta]} $ **(9)**

雅可比矩阵可以用LM方法导出；J can be solved bythe **Levenberg-Marquardt (LM) method**



#### 5.2 Motion Estimation Algorithm

the proposed method only uses depth information associated at frame k − 1

all features taken as the input at frame k without depth as $ {\overline{X}_i^k} $

Features at frame k − 1 are separated into ${X_i}^{k-1}$ and ${\overline{X}_i}^{k-1}$for those with and without depth.

Features that have larger residuals are assigned with smaller weights, and features with residuals larger than a threshold are considered as outliers and assigned with zero weights.

![motion_estimation](.\motion_estimation.png)

#### 5.3 Feature Depth Association

**Background:**

The depth map is projected to the last image frame whose transform to the previous frame is established

**Mechanism:**

New points are added to the depth map upon receiving from depth images or pointcloud.

Only points in front of the camera are kept, and points that are received a certain time ago are forgotten.

the depth map is downsized to maintain a constant point density 保持恒定的点密度

choose angular interval over **Euclidean distance interval** with the consideration that <u>an image pixel represents an angular interval projected into the 3D environment</u>. 考虑到图像像素表示的角度间隔，投影到3D环境中的角度间隔，选择欧几里德距离间隔上的角度间隔。

points are converted into a **spherical coordinate system** coinciding with $ \{C^{k−1} \}$球面坐标系

A point is represented by its radial distance, azimuthal angle, and polar angle. 点由径向距离，方位角和极角表示。

**associate depth with visual features:**

store the depth map in a **2D KD-tree** [29] based on the two angular coordinates

- find three points from the KD-tree that are the closest to the feature
- three points form a local planar patch in the 3D environment
- 3D coordinates of the feature are found by projecting onto the planar patch
- The depth is computed by solving a function **(10)**

Depth info can be derived in two ways:

- **white colored dots** are points on the depth map within a limited range available
- **green colored dots** represent features whose depth are provided by the depth map
- **blue colored dots** are from triangulation.



### VI. BUNDLE ADJUSTMENT

**表现：**

choose one image out of every five images as the bundle adjustment input

**Overview*:**

a trade-off between accuracy and processing time

choose one image out of every five images as the bundle adjustment input.

sequence contains a number of eight images (taken from 40 original images)

the open source library **iSAM** to apply bundle adjustment because it supports user-defined camera models, and can conveniently handle both features with and without available depth.

**Denotion:**

define another representation of the features:  ${\overline{X}_i}^k = \{ {\overline{x}_i}^k, {\overline{y}_i}^k, {z_i}^k \}^T$

the x- and y- entries contain the normalized coordinates, and the z-entry contains the depth.

**Procedure**：

<u>to be continued</u>

**Result:**

publishes refined motion transforms at a low frequency.

result is integrated motion transforms published at the high frequency as the frame to frame motion transforms.



### VII. EXPERIMENTS

**Computation Configuration:**

The feature tracking and bundle adjustment (Section VI) take one core each, and the frame to frame motion estimation (Section V) and depth map registration together consume another core.

*A. Tests with Author-collected Datasets*

The method is compared to two popular **RGB-D visual odometry methods**. 

- Fovis estimates the motionof the camera by tracking image features, and depth is associated to the features from the depth images [20]. 
- DVO is a dense tracking method that  minimizes the photometric error within the overall images [23]. 

both methods use only imaged areas where depth is available, leaving large amount of areas in the visual images being unused.



*B. Tests with KITTI Datasets*

On the KITTI benchmark 2 , our accuracy is comparable to state of the art stereo visual odometry [33], [34]



### VIII. CONCLUSION AND FUTURE WORK

**Problem**:

 insufficient depth information is common for **RGB-D cameras and lidars** which have limited ranges

**Method**: 

exploring both visual features with available and unknown depth.

depth associated to the features in two ways: a) a depth map; b) by triangulation using the previously estimated motion

a bundle adjustment is implemented which refines the frame to frame motion estimates



### Key Idea

- Salient visual features 突出的视觉特征

http://www.scholarpedia.org/article/Visual_salience

- batch optimization 批量优化
- bundle adjustment 光束平差法

https://en.wikipedia.org/wiki/Bundle_adjustment

- triangular geometry 三角几何
- heuristic switch 启发式算法

https://blog.csdn.net/u010159842/article/details/75530645

- skew symmetric matrix 偏斜对称矩阵
- Jacobian matrix [雅可比矩阵](https://zh.wikipedia.org/wiki/%E9%9B%85%E5%8F%AF%E6%AF%94%E7%9F%A9%E9%98%B5)
- [Levenberg-Marquardt (LM) method](https://en.wikipedia.org/wiki/Levenberg%E2%80%93Marquardt_algorithm)
- bisquare 双平方
- [spherical coordinate system](https://en.wikipedia.org/wiki/Spherical_coordinate_system) 球面坐标系

Spherical coordinates (*r*, *θ*, *φ*) : radial distance r, polar angle θ ([theta](https://en.wikipedia.org/wiki/Theta)), and azimuthal angle φ([phi](https://en.wikipedia.org/wiki/Phi)). The symbol ρ ([rho](https://en.wikipedia.org/wiki/Rho)) is often used instead of r.

![360px-3D_Spherical.svg](.\360px-3D_Spherical.svg.png)

计算方法：

![球坐标直角坐标转换](.\球坐标直角坐标转换.png)

-  two popular **RGB-D visual odometry methods**

```
[20] A. Huang, A. Bachrach, P. Henry, M. Krainin, D. Maturana, D. Fox, and N. Roy, “Visual odometry and mapping for autonomous flight using an RGB-D camera,” in Int. Symposium on Robotics Research (ISRR), 2011.
[23] C. Kerl, J. Sturm, and D. Cremers, “Robust odometry estimation for RGB-D cameras,” in IEEE International Conference on Robotics and Automation, Karlsruhe, Germany, 2013.
```

- state of the art stereo visual odometry

```
[33] A. Y. H. Badino and T. Kanade, “Visual odometry by multi-frame feature integration,” in Workshop on Computer Vision for Autonomous Driving (Collocated with ICCV 2013), Sydney, Australia, 2013.
[34] H. Badino and T. Kanade, “A head-wearable short-baseline stereo system for the simultaneous estimation of structure and motion,” in IAPR Conference on Machine Vision Application, Nara, Japan, 2011.
```

