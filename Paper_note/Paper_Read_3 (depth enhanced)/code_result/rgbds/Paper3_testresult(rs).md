[TOC]

---

## 运行结果

```shell
cd /home/philhe/Desktop/test-1youngguncho/
source devel/setup.zsh
roslaunch demo_rgbd demo_rgbd.launch
# 或者下面
cd /home/philhe/Desktop/test-1youngguncho/src/demo_rgbd
roslaunch demo_rgbd.launch

cd /home/philhe/Desktop/VO_src/demo_rgbd/nsh_east_sparse_depth

rosbag play nsh_east_sparse_depth_part1.bag -r 0.5
```
- rviz界面

原始界面: /camera/rgb/image_rect

深度界面:/ camera/depth_registered/image

---

## 结果

### [yoshua-msc-thesis/demo_rgbd_slimmed](https://github.com/yoshua-msc-thesis/demo_rgbd_slimmed)

Changes to make the package compile with OpenCV 3.3.0

编译没有错,但是运行报错

```
[ERROR] [1539441820.811601167]: Ignoring transform for child_frame_id "camera" from authority "unknown_publisher" because of an invalid quaternion in the transform (-nan -nan -nan -nan)
```

### [lkm1321](https://github.com/lkm1321)/[**demo_rgbd_slimmed**](https://github.com/lkm1321/demo_rgbd_slimmed)

编译没有出错,但是featuretracking需要检验

### [grohup](https://github.com/grohup)/[demo_rgbd_slimmed](https://github.com/grohup/demo_rgbd_slimmed)