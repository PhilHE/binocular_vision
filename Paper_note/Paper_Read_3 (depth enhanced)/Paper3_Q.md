[TOC]

---

## 问题

如何理解正交化将$\overline{X}$转变为$X$

雅可比矩阵由LM方法导出？

方程（10）不是很懂

bundle adjustment 过程不是很懂

---

## 代码

原始克隆版本：https://github.com/wpfhtl/demo_rgbd

寻找分支

安装openCV报错

[ubuntu 安装使用多版本opencv](https://blog.csdn.net/heyijia0327/article/details/54575245)

[ubuntu16.04 opencv多版本管理与切换](https://blog.csdn.net/u012986684/article/details/77490824)

#### [youngguncho](https://github.com/youngguncho)/[**demo_rgbd**](https://github.com/youngguncho/demo_rgbd)

```
Kinetic version of DEMO. 
Compile done but lost pose estimation in visualOdometry.cpp
```

可以测试，但丢失了估计在rviz中无法正常显示？

kinetic

也可以用，测试尝试！

#### [yoshua-msc-thesis](https://github.com/yoshua-msc-thesis) / [demo_rgbd](https://github.com/yoshua-msc-thesis/demo_rgbd)

cmakelist.txt

```cmake
find_package(catkin REQUIRED COMPONENTS
  cv_bridge
  roscpp
  sensor_msgs
  std_msgs
  tf
  # pcl_ros
  pcl_catkin
  isam
)
```

报错信息

```
CMake Error at /opt/ros/kinetic/share/catkin/cmake/catkinConfig.cmake:83 (find_package):
  Could not find a package configuration file provided by "pcl_catkin" with
  any of the following names:

    pcl_catkinConfig.cmake
    pcl_catkin-config.cmake

  Add the installation prefix of "pcl_catkin" to CMAKE_PREFIX_PATH or set
  "pcl_catkin_DIR" to a directory containing one of the above files.  If
  "pcl_catkin" provides a separate development package or SDK, be sure it has
  been installed.
Call Stack (most recent call first):
  demo_rgbd/CMakeLists.txt:6 (find_package)
```

添加`pcl_catkin` https://github.com/ethz-asl/pcl_catkin 似乎问题很大?

重新改了pcl,编译isam似乎有问题

```
CMake Error at /opt/ros/kinetic/share/catkin/cmake/catkinConfig.cmake:83 (find_package):
  Could not find a package configuration file provided by "isam" with any of
  the following names:

    isamConfig.cmake
    isam-config.cmake

  Add the installation prefix of "isam" to CMAKE_PREFIX_PATH or set
  "isam_DIR" to a directory containing one of the above files.  If "isam"
  provides a separate development package or SDK, be sure it has been
  installed.
Call Stack (most recent call first):
  demo_rgbd/CMakeLists.txt:6 (find_package)
```

#### [tuandle](https://github.com/tuandle) / [demo_rgbd](https://github.com/tuandle/demo_rgbd)

先测试！不太行

尝试进行切换

```
CMake Error at demo_rgbd/CMakeLists.txt:17 (find_package):
  Could not find a configuration file for package "OpenCV" that is compatible
  with requested version "2".

  The following configuration files were considered but not accepted:

    /opt/ros/kinetic/share/OpenCV-3.3.1-dev/OpenCVConfig.cmake, version: 3.3.1
    /usr/share/OpenCV/OpenCVConfig.cmake, version: 3.3.1
```

opencv2.4.10待验证,安装了后可以编译

启动后报错

```
ERROR: cannot launch node of type [demo_rgbd/featureTracking_rgbd]: can't locate node [featureTracking_rgbd] in package [demo_rgbd]
ERROR: cannot launch node of type [demo_rgbd/visualOdometry_rgbd]: can't locate node [visualOdometry_rgbd] in package [demo_rgbd]
ERROR: cannot launch node of type [demo_rgbd/bundleAdjust_rgbd]: can't locate node [bundleAdjust_rgbd] in package [demo_rgbd]
ERROR: cannot launch node of type [demo_rgbd/processDepthmap_rgbd]: can't locate node [processDepthmap_rgbd] in package [demo_rgbd]
ERROR: cannot launch node of type [demo_rgbd/stackDepthPoint_rgbd]: can't locate node [stackDepthPoint_rgbd] in package [demo_rgbd]
ERROR: cannot launch node of type [demo_rgbd/transformMaintenance_rgbd]: can't locate node [transformMaintenance_rgbd] in package [demo_rgbd]
ERROR: cannot launch node of type [demo_rgbd/registerPointCloud_rgbd]: can't locate node [registerPointCloud_rgbd] in package [demo_rgbd]
```

## 程序运行

### Usage

To run the program, users need to install packages required by the iSAM library, used by the bundle adjustment:

```
sudo apt-get install libsuitesparse-dev libeigen3-dev libsdl1.2-dev
```

Code can be downloaded from [GitHub](https://github.com/jizhang-cmu/demo_rgbd), or following the link on the top of this page. Also, a slimmed down version of the code that does not have the bundle adjustment is available [here](https://github.com/jizhang-cmu/demo_rgbd_slimmed), for running on embedded systems. The program can be started by ROS launch file (available in the downloaded folder), which runs the VO and rivz:

```
roslaunch demo_rgbd.launch
```

For the slimmed down version, use:

```
roslaunch demo_rgbd_slimmed.launch
```

Datasets are available for download from [here](http://www.frc.ri.cmu.edu/~jizhang03/projects.htm), or at the bottom of this page. Please make sure the data files are for the RGBD camera (not camera and lidar) version. With the program running (from the launch file), users can play the data file:

```
rosbag play data_file_name.bag
```

Note that if a slow computer is used, users can try to play the data file at a low speed, e.g. play the data file at half speed:

```
rosbag play data_file_name.bag -r 0.5
```

### Datasets

[NSH East sparse depth](http://www.frc.ri.cmu.edu/~jizhang03/Datasets/nsh_east_sparse_depth.zip)([Video](http://www.frc.ri.cmu.edu/~jizhang03/Videos/nsh_east_sparse_depth.mp4)): outdoor with limited depth information

![nsh_east_sparse_depth.jpg](http://wiki.ros.org/demo_rgbd?action=AttachFile&do=get&target=nsh_east_sparse_depth.jpg)