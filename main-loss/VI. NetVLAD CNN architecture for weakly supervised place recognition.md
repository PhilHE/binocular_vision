[TOC]

# VLAD 

> vector of locally aggregated descriptors

## 原始版本VLAD

> Aggregating local image descriptors into compact codes

《Aggregating local descriptors into a compact image representation》论文笔记 
这篇论文中提出了一种新的图片表示方法，也就是VLAD特征，并把它用于大规模图片检索中，得到了很好的实验结果。 

目前，BOF在图片检索和分类中应用广泛，首先是因为BOF是基于比较powerful的local特征(如SIFT)得来的，所以表达能力很强；其次是因为计算BOF过程中用到的kmeans也是根据样本在样本空间的距离来聚类的，所以，BOF也可以输入SVM这类基于样本间隔的分类器得到较好的效果。但是在数据量很大的情况下，由于vocabulary大小的限制，BOF的特征表达会越来越粗略，特征信息损失较多，使得搜索精度降低。 
这篇论文在大数据量的图片搜索问题上，做了3方面的优化： 
1，优化特征表示方法，使用VLAD特征； 
2，对降维方法(PCA)做改进 
3，对索引方法(ADC)做改进 

论文的主要贡献

1，基于BOF和Fisher vector这这两种聚合local特征的方法提出了VLAD特征； 

2，对降维方法和索引方法做优化，而这两个优化是trade-off的，也就是此消彼长的关系，所以，论文中通过大量实验得到一个平衡值。

---

VLAD可以理解为是BOF和fisher vector的折中 
BOF是把特征点做kmeans聚类，然后用离特征点最近的一个聚类中心去代替该特征点，损失较多信息； 
Fisher vector是对特征点用GMM建模，GMM实际上也是一种聚类，只不过它是考虑了特征点到每个聚类中心的距离，也就是用所有聚类中心的线性组合去表示该特征点，在GMM建模的过程中也有损失信息； 
VLAD像BOF那样，只考虑离特征点最近的聚类中心，VLAD保存了每个特征点到离它最近的聚类中心的距离； 
像Fisher vector那样，VLAD考虑了特征点的每一维的值，对图像局部信息有更细致的刻画； 

而且VLAD特征没有损失信息。



## I. VLAD

### 主要内容

#### 思路

mean average precision (mAP)

![mAP](C:\Users\admin\Desktop\Opensource _Paper_Code\NetVLAD\mAP.png)

signed square rooting (SSR)

region of interest (ROI)

L2归一化范数

#### 模块

在各个方面改进了标准VLAD描述符，即**聚类中心自适应，内部归一化和MultiVLAD**。



$ \dfrac{1}{C^{(1)}} \sum\limits_i (x_{k,i}^{(1)} \mu_k)^T \dfrac{1}{C^{(2)}} \sum\limits_j (x_{k,i}^{(2)} \mu_k)^T$

#### 输入输出



#### 未来工作？

VLAD不存储任何空间信息！

---

## II. NetVLAD: CNN architecture for weakly supervised place recognition

### 主要内容

#### 思路



#### 模块

- softMax函数

[softmax函数详解](https://www.cnblogs.com/alexanderkun/p/8098781.html)

**softmax用于多分类过程中**，它将多个神经元的输出，映射到（0,1）区间内，可以看成概率来理解，从而来进行多分类！假设我们有一个数组，V，Vi表示V中的第i个元素，那么这个元素的softmax值就是softmax函数如下： 

$ S_{i} = \dfrac{e^i}{\sum_j e^j} $

其可以看成sigmoid函数在多元分布中的一个推广 

#### 输入输出

---

## 主要论文

### ALL ABOUT VLAD文章内容

#### Abstract

The objective of this paper is **large scale object instance retrieval**, given a query image. A starting point of such systems is feature detection and description, for example using SIFT. The focus of this paper, however, is towards very large scale retrieval where, due to storage requirements, very **compact image descriptors** are required and no information about the original SIFT descriptors can be accessed directly at run time. We start from VLAD, the state-of-the art compact descriptor introduced by J´egou et al.[8] for this purpose, and make three novel contributions: 

first, we show that a simple change to the **normalization method** significantly improves retrieval performance; 

second, we show that vocabulary adaptation can substantially alleviate problems caused when images are added to the dataset after initial vocabulary learning. These two methods set a new stateof-the-art over all benchmarks investigated here for both mid-dimensional (20k-D to 30k-D) and small (128-D) descriptors. 

Our third contribution is a **multiple spatial VLAD representation**, MultiVLAD, that allows the retrieval and localization of objects that only extend over a small part of an image (again without requiring use of the original image SIFT descriptors).

本文的目的是给出一个查询图像的**大规模对象实例检索**。这种系统的起点是特征检测和描述，例如使用SIFT。然而，本文的重点是非常大规模的检索，其中由于存储要求，需要非常**紧凑的图像描述符**，并且在运行时不能直接访问关于原始SIFT描述符的信息。我们从VLAD开始，这是由J'egou等人[8]引入的最先进的紧凑描述符。为此目的，并做出三个新的贡献：

首先，我们表明对规范化方法的简单改变显着提高了检索性能;

第二，我们表明词汇适应可以大大减轻在初始词汇学习后将图像添加到数据集时引起的问题。这两种方法在这里研究的所有基准测试中都为中维（20k-D到30k-D）和小（128-D）描述符设置了一种新的现有技术。

我们的第三个贡献是**多空间VLAD表示，MultiVLAD，它允许检索和定位仅扩展到图像的一小部分**的对象（同样不需要使用原始图像SIFT描述符）。

#### I. Introduction 

The area of large scale particular object retrieval has  seen a steady train of improvements in performance over the last decade. Since the original introduction of the **bag of visual words (BoW) formulation** [16], there have been many notable contributions that have <u>enhanced the descriptors [1, 15, 19], reduced quantization loss [6, 14, 18], and improved recall</u> [1, 3, 4]. However, one of the most significant contributions in this area has been the introduction of the **Vector of Locally Aggregated Descriptors (VLAD) by J´egou et al.[8]. This image descriptor was designed to be very low dimensional (e.g. 16 bytes per image) so that all the descriptors for very large image datasets (e.g. 1 billion images) could still fit into main memory** (and thereby avoid expensive hard disk access). Its introduction has opened up a new research theme on the trade-off between memory footprint of an image descriptor and retrieval performance, e.g. measured by average precision. 

We review VLAD in section 2.1, but here mention that VLAD, like visual word encoding, starts by vector quantizing a locally invariant descriptor such as SIFT. It d**iffers from the BoW image descriptor by recording the difference from the cluster center, rather than the number of SIFTs assigned to the cluster**. It inherits some of the invariances of the original SIFT descriptor, such as in-plane rotational invariance, and is somewhat tolerant to other transformations such as image scaling and clipping. Another difference from the standard BoW approach is that **VLAD retrieval systems generally preclude the use of the original local descriptors**. These are used in BoW systems for spatial verification and reranking [6, 13], but require too much storage to be held in memory on a single machine for very large image datasets. VLAD is similar in spirit to the earlier Fisher vectors [11], as both record aspects of the distribution of SIFTs assigned to a cluster center. 

在过去十年中，大规模特定对象检索领域已经取得了稳定的性能改进。自从最初引入**视觉词袋（BoW）**制定[16]以来，已经有许多值得注意的贡献，它们<u>增强了描述符[1,15,19]，减少了量化损失[6,14,18]</u>，并得到了改进。回忆[1,3,4]。然而，该领域最重要的贡献之一是J'egou等[8]引入了**局部聚合描述符向量（VLAD）。该图像描述符被设计成非常低维度（例如，每个图像16个字节），使得非常大的图像数据集（例如10亿个图像）的所有描述符仍然可以适合主存储器**（从而避免昂贵的硬盘访问）。它的引入为图像描述符的内存占用与检索性能之间的权衡开辟了一个新的研究主题。以平均精度测量。

我们在2.1节中回顾了VLAD，但是这里提到VLAD与视觉字编码一样，通过矢量量化局部不变量描述符（如SIFT）开始。它**与BoW图像描述符的不同之处在于记录了与集群中心的差异，而不是分配给集群的SIFT数量**。它继承了原始SIFT描述符的一些不变性，例如平面内旋转不变性，并且在某种程度上可以容忍其他变换，例如图像缩放和裁剪。与标准BoW方法的另一个区别是**VLAD检索系统通常排除使用原始本地描述符**。这些在<u>BoW系统中用于空间验证和重新排名[6,13]，但是对于非常大的图像数据集，需要在单个机器的内存中保存太多存储空间</u>。 VLAD在精神上类似于早期的Fisher矢量[11]，因为两个SIFT分配到集群中心的记录方面都是如此。

As might be expected, papers are now investigating how to improve on the original VLAD formulation [2, 5]. This paper is also aimed at improving the performance of VLAD. We make three contributions:

1. **Intra-normalization:** We propose a n**ew normalization scheme for VLAD** that addresses the problem of burstiness [7], where a few large components of the VLAD vector can adversely dominate the similarity computed between VLADs. The new normalization is simple, and always improves retrieval performance. 
2. **Multi-VLAD:** We study the benefits of recording **multiple VLADs for an image and show that retrieval performance is improved for small objects** (those that cover only a small part of the image, or where there is a significant scale change from the query image). Furthermore, we propose a method of sub-VLAD localization where the window corresponding to the object instance is estimated at a finer resolution than the the VLAD tiling.
3. **Vocabulary adaptation**: We investigate the problem of vocabulary sensitivity, where a vocabulary trained on one dataset, A, is used to represent another dataset B, and the performance is inferior to using a vocabulary trained on B. We propose an efficient, simple, method for improving VLAD descriptors via vocabulary adaptation, without the need to store or recompute any local descriptors in the image database

正如所料，论文正在研究如何**改进最初的VLAD方程[2,5]。本文还旨在提高VLAD的性能**。我们做出三点贡献：

1. 帧内归一化：我们提出了一种新的**VLAD归一化方案**，它解决了突发性问题[7]，其中VLAD向量的一些大分量可能不利地支配VLAD之间计算的相似性。新的规范化很简单，并且总能提高检索性能。

2. Multi-VLAD：我们研究了**为图像记录多个VLAD的好处，**并显示小型对象（仅覆盖图像的一小部分，或者查询中存显着的比例变化）的检索性能得到改善图片）。此外，我们提出了一种子VLAD定位方法，其中对应于对象实例的窗口以比VLAD平铺更精细的分辨率估计。
3. 词汇适应：我们研究词汇敏感性的问题，其中在一个数据集A上训练的词汇表用于表示另一个数据集B，并且表现不如使用在B上训练的词汇表。我们提出了一个有效，简单的词汇表。 通过**词汇自适应改进VLAD描述符的方法**，无需在图像数据库中存储或重新计算任何本地描述符

The first two contributions are targeted at improving VLAD performance. The first improves retrieval in general, and the second partially overcomes an important deficiency – that VLAD has inferior invariance to changes in scale (compared to a BoW approach). The third contribution addresses a problem that arises in real-world applications where, for example, image databases grow with time and the original vocabulary is incapable of representing the additional images well. 

In sections 3–5 we describe each of these methods in detail and demonstrate their performance gain over earlier VLAD formulations, using the **Oxford Buildings 5k and Holidays image dataset benchmarks** as running examples. The methods are combined and compared to the state of the art for larger scale retrieval (Oxford 105k and Flickr1M) in section 6.

前两个贡献旨在提高VLAD性能。 第一种方法总体上改进了检索，第二种方法部分地克服了一个重要的缺陷--VLAD在规模变化方面具有较差的不变性（与BoW方法相比）。 第三个贡献解决了在现实世界的应用中出现的问题，例如，图像数据库随着时间而增长并且原始词汇表不能很好地表示附加图像。

在第3-5节中，我们详细描述了这些方法中的每一种，并使用**Oxford Buildings 5k和Holidays图像数据集**基准作为运行示例，展示了它们相对于早期VLAD配方的性能增益。 将这些方法组合并与第6节中用于更大规模检索的技术（Oxford 105k和Flickr1M）进行比较。

#### II. VLAD review, datasets and baselines

We first describe the original VLAD computation and subsequent variations, and then briefly overview the datasets that will be used for performance evaluation and those that will be used for vocabulary building (obtaining the cluster centers required for VLAD computation).

我们首先描述原始的VLAD计算和随后的变化，然后简要概述将用于性能评估的数据集和将用于词汇构建的数据集（获得VLAD计算所需的集群中心）。

2.1. VLAD

VLAD is constructed as follows: regions are extracted from an image using an affine invariant detector, and described using the 128-D SIFT descriptor. Each descriptor is then assigned to the closest cluster of a vocabulary of size k (where k is typically 64 or 256, so that clusters are quite coarse). For each of the k clusters, the residuals (vector differences between descriptors and cluster centers) are accumulated, and the k 128-D sums of residuals are concatenated into a single k × 128 dimensional descriptor; we refer to it as the unnormalized VLAD. Note, VLAD is similar to other descriptors that record residuals such as Fisher vectors [11] and super-vector coding [20]. The relationship between Fisher vectors and VLAD is discussed in [12]. In the original scheme [8] the VLAD vectors are L2 normalized. Subsequently, a signed square rooting (SSR) normalization was introduced [5, 9], following its use by Perronnin et al.[12] for Fisher vectors. To obtain the SSR normalized VLAD, each element of an unnormalized VLAD is sign square rooted (i.e.anelement xi is transformed into  and the transformed vector is L2 normalized. We will compare with both of these normalizations in the sequel, and use them as baselines for our approach. Chen et al.[2] propose a different normalization scheme for the residuals and also investigate omitting SIFT descriptors that lie close to cluster boundaries. J´egou and Chum [5] extend VLAD in two ways: 

first, by using PCA and whitening to decorrelate a low dimensional representation; and 

second, by using multiple (four) clusterings to overcome quantization losses. 

Both give a substantial retrieval performance improvement for negligible additional computational cost, and we employ them here.

VLAD构造如下：使用仿射不变量检测器从图像中提取区域，并使用128-D SIFT描述符进行描述。然后将每个描述符分配给大小为k的词汇表的最接近的簇（其中k通常为64或256，因此簇非常粗糙）。对于k个簇中的每一个，累积残差（描述符和聚类中心之间的矢量差），并且将k 128-D残差和连接成单个k×128维描述符;我们将其称为非标准化VLAD。注意，VLAD类似于记录残差的其他描述符，例如Fisher向量[11]和超向量编码[20]。 Fisher矢量和VLAD之间的关系在[12]中讨论。在原始方案[8]中，VLAD向量被L2归一化。随后，在Perronnin等[12]的使用之后，引入了带符号的平方根（SSR）归一化[5,9]。对于Fisher矢量。为了获得SSR归一化VLAD，非标准化VLAD的每个元素都是符号平方根（即，元素xi被转换为并且转换后的向量被L2归一化。我们将在续集中与这两个归一化进行比较，并将它们用作基线我们的方法.Chen等人[2]提出了一个不同的残差归一化方案，并且还研究了省略靠近簇边界的SIFT描述符.J'egou和Chum [5]以两种方式扩展VLAD：

第一，使用PCA并且，通过使用多个（四个）聚类来克服量化损失，使白化去相关;

以及通过使用多个（四个）聚类来克服量化损失。

两者都提供了实质上的检索性能改进，可忽略不计的额外计算成本，并且我们在这里使用它们。

2.2. Benchmark datasets and evaluation procedure

The performance is measured on two standard and publicly available image retrieval benchmarks, **Oxford buildings and Holidays**. For both, a set of predefined queries with hand-annotated ground truth is used, and the retrieval performance is measured in terms of mean average precision (mAP). 

Oxford buildings [13] contains 5062 images downloaded from Flickr, and is often referred to as Oxford 5k.There are 55 queries specified by an image and a rectangular region of interest. To test large scale retrieval, it is extended with a 100k Flickr images, forming the Oxford 105k dataset. 

Holidays [6] contains 1491 high resolution images containing personal holiday photos with 500 queries. For large scale retrieval, it is appended with 1 million Flickr images (Flickr1M [6]), forming Holidays+Flickr1M. We follow the standard experimental scenario of [5]for all benchmarks: for Oxford 5k and 105k the detector and SIFT descriptor are computed as in [10]; while for Holidays(+Flickr1M) the publicly available SIFT descriptors are used.

**Vocabulary sources.** Three different datasets are used for vocabulary building (i.e. clustering on SIFTs):

(i) Paris 6k [14], which is analogous to the Oxford buildings dataset, and is often used as an independent dataset from the Oxford buildings [1, 3, 5, 14]; 

(ii) Flickr60k [6], which contains 60k images downloaded from Flickr, and is used as an independent dataset from the Holidays dataset [6, 7, 8]; 

(iii) ‘no-vocabulary’, which simply uses the first k (where k is the vocabulary size) SIFT descriptors from the Holidays dataset. As k is typically not larger than 256 whereas the smallest dataset (Holidays) contains 1.7 million SIFT descriptors, this vocabulary can be considered independent from all datasets.

性能测量是在两个标准和公开可用的图像检索基准，Oxford buildings and Holidays。对于两者，使用具有手工注释的基础事实的一组预定义查询，并且以平均平均精度（mAP）来测量检索性能。

牛津大厦[13]包含从Flickr下载的5062个图像，并且通常被称为牛津5k。由图像和感兴趣的矩形区域指定55个查询。为了测试大规模检索，它使用100k Flickr图像进行扩展，形成Oxford 105k数据集。

假期[6]包含1491个高分辨率图像，其中包含500个查询的个人假日照片。对于大规模检索，它附加了100万Flickr图像（Flickr1M [6]），形成假日+ Flickr1M。对于所有基准测试，我们遵循[5]的标准实验场景：对于Oxford 5k和105k，检测器和SIFT描述符的计算方法如[10]中所述;而对于假期（+ Flickr1M），使用公开可用的SIFT描述符。

词汇来源。三个不同的数据集用于词汇构建（即SIFT上的聚类）:

i）Paris 6k [14]，类似于牛津建筑数据集，通常用作牛津大厦的独立数据集[1,3]， 5,14]; 

ii）Flickr60k [6]，其中包含从Flickr下载的60k图像，并用作Holidays数据集的独立数据集[6,7,8]

iii）'no-vocabulary'，其仅使用来自Holidays数据集的第一个k（其中k是词汇量大小）SIFT描述符。由于k通常不大于256，而最小的数据集（假日）包含170万个SIFT描述符，因此可以认为该词汇表独立于所有数据集。

#### III. Vocabulary adaptation

![VLAD1](C:\Users\admin\Desktop\Opensource _Paper_Code\NetVLAD\VLAD1.png)

*Figure 1: VLAD similarity measure under different clusterings. The Voronoi cells illustrate the coarse clustering used to construct VLAD descriptors. Red crosses and blue circles correspond to local descriptors extracted from two different images, while the red and blue arrows correspond to the sum of their residuals (differences between descriptors and the cluster center). Assume the clustering in (a) is a good one (i.e. it is representative and consistent with the dataset descriptors), while the one in (b) is not. By changing the clustering from (a) to (b), the sign of the similarity between the two images (from the cosine of the angle between the residuals) changes dramatically, from negative to positive. However, by performing cluster center adaptation the residuals are better estimated (c), thus inducing a better estimate of the image similarity which is now consistent with the one induced by the clustering in (a).*

图1：不同聚类下的VLAD相似性度量。 Voronoi单元格说明了用于构造VLAD描述符的粗聚类。 红色十字和蓝色圆圈对应于从两个不同图像提取的局部描述符，而红色和蓝色箭头对应于其残差的总和（描述符和聚类中心之间的差异）。 假设（a）中的聚类是一个好的聚类（即它是代表性的并且与数据集描述符一致），而（b）中的聚类则不是。 通过将聚类从（a）改变为（b），两个图像之间的相似性符号（来自残差之间的角度的余弦）从负到正变化。 然而，通过执行聚类中心自适应，更好地估计残差（c），从而引起图像相似性的更好估计，其现在与由（a）中的聚类诱导的图像相似性一致。

In this section we introduce **cluster adaptation to improve retrieval performance for the case** where the cluster centers used for VLAD are not consistent with the dataset for example they were obtained on a different dataset or because new data has been added to the dataset. As described earlier (section 2.1), VLAD is constructed by aggregating differences between local descriptors and coarse cluster centers, followed by L2 normalization. For the dataset used to learn the clusters (by k-means) the centers are consistent in that the mean of all vectors assigned to a cluster over the entire dataset is the cluster center. For an individual VLAD (from a single image) this is not the case, or course, and it is also not the case, in general, for VLADs computed over a different dataset. 

As will be seen below the inconsistency can severely impact performance. An ideal solution would be to recluster on the current dataset, but this is costly and requires access to the original SIFT descriptors. Instead, the method we propose alleviates the problem without requiring reclustering.

The similarity between VLAD descriptors is measured as the scalar product between them, and this decomposes as the sum of scalar products of aggregated residuals for each coarse cluster independently. Consider a contribution to the similarity for one particular coarse cluster k. We denote with x(1) k and x(2) k the set of all descriptors in image 1 and 2, respectively, which get assigned to the same coarse cluster k. The contribution to the overall similarity of the two VLAD vectors is then equal to:

在本节中，我们将介绍群集自适应，以提高用于VLAD的群集中心与数据集不一致的情况下的检索性能 例如，它们是在不同的数据集上获得的，或者因为新数据已添加到数据集中。如前所述（第2.1节），VLAD是通过聚合局部描述符和粗聚类中心之间的差异，然后进行L2归一化来构造的。对于用于学习聚类的数据集（通过k均值），中心是一致的，因为在整个数据集中分配给聚类的所有向量的平均值是聚类中心。对于单个VLAD（来自单个图像），情况并非如此，并且通常情况下，对于在不同数据集上计算的VLAD也不是这种情况。

如下所示，不一致性会严重影响性能。理想的解决方案是在当前数据集上进行重新集群，但这样做成本很高，需要访问原始的SIFT描述符。相反，我们提出的方法可以在不需要重新集群的情况下缓解问题。

VLAD描述符之间的相似性被测量为它们之间的标量积，并且这作为每个粗簇的聚合残差的标量积的总和独立地分解。考虑对一个特定粗簇k的相似性的贡献。我们用x（1）k和x（2）k分别表示图像1和2中的所有描述符的集合，其被分配给相同的粗集群k。然后，对两个VLAD向量的总体相似性的贡献等于：

$ \dfrac{1}{C^{(1)}} \sum\limits_i (x_{k,i}^{(1)} \mu_k)^T \dfrac{1}{C^{(2)}} \sum\limits_j (x_{k,i}^{(2)} \mu_k)^T$ [1]

where μk is the centroid of the cluster, and C(1) and C(2) are normalizing constants which ensure all VLAD descriptors have unit norm. Thus, the similarity measure induced by the VLAD descriptors is increased if the scalar product between the residuals is positive, and decreased otherwise. For example, the sets of descriptors illustrated in figure 1a are deemed to be very different (they are on opposite sides of the cluster center) thus giving a negative contribution to the similarity of the two images.

It is clear that the VLAD similarity measure is strongly affected by the cluster center. For example, if a different center is used (figure 1b), the two sets of descriptors are now deemed to be similar thus yielding a positive contribution to the similarity of the two images. Thus, a different clustering can yield a completely different similarity value. We now introduce cluster center adaptation to improve residual estimates for an inconsistent vocabulary, namely, using new adapted cluster centers ˆμk that are consistent when computing residuals (equation (1)), instead of the original cluster centers μk. The algorithm consists of two steps: 

(i) compute the adapted cluster centers ˆμk as the mean of all local descriptors in the dataset which are assigned to the same cluster k; 

(ii) recompute all VLAD descriptors by aggregating differences between local descriptors and the adapted centers ˆμk. 

Note that step (ii) can be performed without actually storing or recomputing all local descriptors as their assignment to clusters remains unchanged and thus it is sufficient only to store the descriptor sums for every image and each cluster.

其中**μk是簇的质心，C（1）和C（2）是归一化常数**，它们确保所有VLAD描述符都具有单位范数。因此，如果残差之间的标量积为正，则VLAD描述符引起的相似性度量增加，否则减小。例如，图1a中所示的描述符集被认为是非常不同的（它们位于聚类中心的相对侧），因此对两个图像的相似性给出了负面贡献。

很明显，**VLAD相似性度量受到群集中心的强烈影响。例如，如果使用不同的中心（图1b），则现在认为两组描述符相似，从而对两个图像的相似性产生积极贡献**。因此，不同的聚类可以产生完全不同的相似性值。我们现在引入聚类中心自适应来改进不一致词汇表的残差估计，即，**使用新的自适应聚类中心μk，其在计算残差时是一致的（等式（1）），而不是原始聚类中心μk**。该算法包括两个步骤：

i）将适应的聚类中心μk计算为数据集中分配给相同聚类k的所有局部描述符的平均值;

ii）通过聚合局部描述符和适应的中心μk之间的差异来重新计算所有VLAD描述符。

注意，可以在不实际存储或重新计算所有局部描述符的情况下执行步骤（ii），因为它们对簇的分配保持不变，因此仅存储每个图像和每个簇的描述符和就足够了。

Figure 1c illustrates the improvement achieved with center adaptation, as now residuals, and thus similarity scores, are similar to the ones obtained using the original clustering in figure 1a. Note that for an adapted clustering the cluster center is indeed equal to the mean of all the descriptors assigned to it from the dataset. Thus, our cluster adaptation scheme has no effect on VLADs obtained using consistent clusters, as desired. To illustrate the power of the adaptation, a simple test is performed where the Flickr60k vocabulary is used for the Oxford 5k dataset, and the difference between the original vocabulary and the adapted one measured. The mean magnitude of the displacements between the k = 256 adapted and original cluster centers is 0.209, which is very large keeping in mind that RootSIFT descriptors [1]themselves all have a unit magnitude. For comparison, when the Paris vocabulary is used, the mean magnitude of the difference is only 0.022.

Results. Figure 2 shows the improvement in retrieval performance obtained when using cluster center adaptation (adapt) compared to the standard VLAD under various dataset sources for the vocabulary. Center adaptation improves results in all cases, especially when the vocabulary was computed on a vastly different image database or not computed at all. For example, on Holidays with Paris vocabulary the mAP increases by 9.7%, from 0.432 to 0.474; while for the no-vocabulary case, the mAP improves by 34%, from 0.380 to 0.509. The improvement is smaller when the Flickr60k vocabulary is used since the distribution of descriptors is more similar to the ones from the Holidays dataset, but it still exists: 3.2% from 0.597 to 0.616. The improvement trends are similar for the Oxford 5k benchmark as well.

Application in large scale retrieval. Consider the case of real-world large-scale retrieval where images are added to the database with time. This is the case, for example, with users uploading images to Flickr or Facebook, or Google indexing images on new websites. In this scenario, one is forced to use a fixed precomputed vocabulary since it is impractical (due to storage and processing requirements) to recompute too frequently as the database grows, and reassign all descriptors to the newly obtained clusters. In this case, it is quite likely that the obtained clusters are inconsistent, thus inducing a bad VLAD similarity measure. Using cluster center adaptation fits this scenario perfectly as it provides a way of computing better similarity estimates without the need to recompute or store all local descriptors, as descriptor assignment to clusters does not change.

图1c示出了利用中心自适应实现的改进，因为现在残差和因此相似性得分类似于使用图1a中的原始聚类获得的残差。请注意，对于经过调整的聚类，聚类中心确实等于从数据集分配给它的所有描述符的均值。因此，根据需要，我们的簇适应方案对使用一致簇获得的VLAD没有影响。为了说明自适应的力量，进行了一个简单的测试，其中Flickr60k词汇表用于Oxford 5k数据集，以及原始词汇与测量的自适应词汇之间的差异。 k = 256个适应的和原始聚类中心之间的位移的平均幅度是0.209，这是非常大的，记住RootSIFT描述符[1]本身都具有单位幅度。相比之下，当使用巴黎词汇时，差异的平均幅度仅为0.022。

结果。**图2显示了在词汇表的各种数据集源下，与标准VLAD相比，使用群集中心自适应（自适应）时获得的检索性能的改进。中心自适应改善了所有情况下的结果，特别是当词汇表在一个截然不同的图像数据库上计算或根本不计算时**。例如，在Paris with Paris词汇表中，mAP增加了9.7％，从0.432增加到0.474;而对于无词汇量的情况，mAP提高了34％，从0.380增加到0.509。当使用Flickr60k词汇表时，改进较小，因为描述符的分布与假日数据集的分布更相似，但它仍然存在：从0.597到0.616的3.2％。 Oxford 5k基准测试的改进趋势也是类似的。

应用于大规模检索。考虑现实世界大规模检索的情况，其中图像随时间添加到数据库中。例如，用户将图像上传到Flickr或Facebook，或谷歌在新网站上索引图像就是这种情况。在这种情况下，一个人被迫使用固定的预计算词汇表，因为在数据库增长时过于频繁地重新计算是不切实际的（由于存储和处理要求），并将所有描述符重新分配给新获得的集群。在这种情况下，很可能获得的簇不一致，从而引起不良的VLAD相似性度量。**使用群集中心自适应完全适合这种情况，因为它提供了一种计算更好的相似性估计的方法，而无需重新计算或存储所有本地描述符**，因为对群集的描述符分配不会改变。

*Figure 2: Retrieval performance. Six methods are compared, namely: (i) baseline: the standard VLAD, (ii) intra-normalization, innorm (section 4), (iii) center adaptation, adapt (section 3), (iv) adapt followed by innorm, (v) baseline: signed square rooting SSR, (vi) aided baseline: adapt followed by SSR. Each result corresponds to the mean result obtained from four different test runs (corresponding to four different clusterings), while error bars correspond to one standard deviation. The results were generated using RootSIFT [1] descriptors and vocabularies of size k =256.*

图2：检索性能。 比较了六种方法，即：

i）基线：标准VLAD，

ii）内部归一化，不变形（第4节），

iii）中心适应，适应（第3节），

iv）适应后跟变形，

v）基线：有符号平方根SSR，

vi）辅助基线：适应后跟SSR。

 每个结果与从四个不同的测试运行（对应于四个不同的聚类）获得的平均结果相对应，而误差条对应于一个标准偏差。 结果是使用RootSIFT [1]描述符和大小为k = 256的词汇表生成的。

#### IV. Intra-normalization

In this section, it is shown that **current methods for normalizing VLAD descriptors**, namely simple L2 normalization [8] and signed square rooting [12], are prone to putting too much weight on bursty visual features, resulting in a suboptimal measure of image similarity. To alleviate this problem, we propose a new method for VLAD normalization.

The problem of bursty visual elements was first noted in the bag-of-visual-words (BoW) setting [7]: <u>a few artificially large components in the image descriptor vector (for example resulting from a repeated structure in the image such as a tiled floor) can strongly affect the measure of similarity between two images, since the contribution of other important dimensions is hugely decreased</u>. This problem was alleviated by discounting large values by element-wise square rooting the BoWvectors and re-normalizing them. In a similar manner **VLADs are signed square root (SSR) normalized** [5, 9]. Figure 3 shows the effects these normalizations have on the average energy carried by each dimension in a VLAD vector.

We propose here **a new normalization, termed intranormalization**, where the sum of residuals is L2 normalized within each VLAD block (i.e. sum of residuals within a coarse cluster) independently. As in the original VLAD and SSR, this is followed by L2 normalization of the entire vector. This way, regardless of the amount of bursty image features their effect on VLAD similarity is localized to their coarse cluster, and is of similar magnitude to all other contributions from other clusters. While SSR reduces the burstiness effect, it is limited by the fact that it only discounts it. In contrast, intra-normalization fully suppresses bursts, as witnessed in figure 3c which shows absolutely no peaks in the energy spectrum.

在本节中，显示了用于**归一化VLAD描述符的当前方法**，即简单的L2归一化[8]和带符号的平方根SSR[12]，倾向于过多地强调突发的视觉特征，导致图像相似性的次优度量。为了缓解这个问题，我们提出了一种用于VLAD归一化的新方法。

突发性视觉元素的问题首先在视觉词袋（BoW）设置中被注意到[7]：<u>图像描述符向量中的一些人为大的组件（例如由图像中的重复结构产生的）平铺地板）可以强烈影响两个图像之间的相似性度量，因为其他重要维度的贡献大大减少</u>。通过以元素方式对BoWvectors进行平方并对它们进行重新规范化来对大值进行折扣来缓解这个问题。以类似的方式，**VLAD是带符号的平方根（SSR）归一化[5,9]**。图3显示了这些标准化对VLAD向量中每个维度所承载的平均能量的影响。

我们在此提出一种新的归一化，称为**内部归一化**，其中**残差的总和是在每个VLAD块内标准化的L2（即粗簇内残差的总和）**。与最初的VLAD和SSR一样，接着是整个矢量的L2归一化。这样，无论突发图像特征的数量如何，它们对VLAD相似性的影响都局限于其粗集群，并且与其他集群的所有其他贡献具有相似的幅度。虽然SSR降低了突发性效果，但它受到的限制只是折扣它。相反，内部归一化完全抑制了突发，如图3c所示，其显示能量谱中绝对没有峰值。

**Discussion.** The geometric interpretation of intranormalization is that the similarity of two VLAD vectors depends on the angles between the residuals in corresponding clusters. This follows from the scalar product of equation (1): since the residuals are now L2 normalized the scalar product depends only on the cosine of the differences in angles of the residuals, not on their magnitudes. Chen et al.[2] have also proposed an alternative normalization where the per-cluster mean of residuals is computed instead of the sum. The resulting representation still depends on the magnitude of the residuals, which is strongly affected by the size of the cluster, whereas in intra-normalization it does not. Note that all the arguments made in favor of cluster center adaptation (section 3) are unaffected by intranormalization. Specifically, only the values of C(1) and C(2) change in equation (1), and not the dependence of the VLAD similarity measure on the quality of coarse clustering which is addressed by cluster center adaptation.

Results. As shown in figure 2, intra-normalization (innorm) combined with center adaptation (adapt) always improves retrieval performance, and consistently outperforms other VLAD normalization schemes, namely the original VLAD with L2 normalization and SSR. Center adaptation with intra-normalization (adapt+innorm) significantly outperforms the next best method (which is adapt+SSR); the average relative improvement on Oxford 5k and Holidays is 4.7% and 6.6%, respectively. Compared to SSR without center adaptation our improvements are even more evident: 35.5% and 27.2% on Oxford 5k and Holidays, respectively.

**讨论。**非正规化的几何解释是两个VLAD向量的相似性取决于相应聚类中残差之间的角度。这是从等式（1）的标量乘积得出的：由于残差现在是L2归一化的，因此标量乘积仅取决于残差角度差的余弦，而不取决于它们的大小。陈等人[2]还提出了一种替代归一化，其中计算残差的每簇均值而不是总和。得到的表示仍然取决于残差的大小，其受到群集大小的强烈影响，而在内部归一化则不然。请注意，所有支持集群中心自适应的参数（第3节）都不受非规范化的影响。具体地，只有C（1）和C（2）的值在等式（1）中改变，而不是VLAD相似性度量对通过聚类中心自适应解决的粗聚类的质量的依赖性。

结果。如图2所示，内部归一化（innorm）与中心自适应（自适应）相结合总能提高检索性能，并且始终优于其他VLAD归一化方案，即具有L2归一化和SSR的原始VLAD。具有内部归一化的中心适应（adapt + innorm）明显优于下一个最佳方法（即适应+ SSR）;牛津5k和假期的平均相对改善率分别为4.7％和6.6％。与没有中心调整的SSR相比，我们的改进更加明显：牛津5k和假期分别为35.5％和27.2％。

#### V. Multiple VLAD descriptors

In this section we investigate the benefits of tiling an image with VLADs, instead of solely representing the image by a single VLAD. As before, our constraints are the memory footprint and that any performance gain should not involve returning to the original SIFT descriptors for the image. We target objects that only cover a small part of the image (VLAD is known to have inferior performance for these compared to BoW), and describe first how to improve their retrieval, and second how to predict their localization and scale (despite the fact that VLAD does not store any spatial information).

The multiple VLAD descriptors (MultiVLAD) are **extracted on a regular 3 × 3 grid at three scales**. <u>14 VLAD descriptors are extracted: nine (3 × 3) at the finest scale, four (2 × 2) at the medium scale (each tile is formed by 2×2 tiles from the finest scale), and one covering the entire image</u>. At run time, given a query image and region of interest (ROI) covering the queried object, a single VLAD is computed over the ROI and matched across database VLAD descriptors. An image in the database is assigned a score equal to the maximum similarity between any of its VLAD descriptors and the query. 

As will be shown below, <u>computing VLAD descriptors at fine scales enables retrieval of small objects, but at the cost of increased storage (memory) requirements</u>. However, with 20 bytes per image [8], 14 VLADs per image amounts to 28 GB for a 100 million images, which is still a manageable amount of data that can easily be stored in the main memory of a commodity server. 

在本节中，我们将研究使用VLAD平铺图像的好处，而不是仅通过单个VLAD表示图像。和以前一样，我们的约束是内存占用，任何性能增益都不应该涉及返回图像的原始SIFT描述符。我们**针对仅覆盖图像的一小部分的对象（已知VLAD与BoW相比具有较差的性能），并且首先描述如何改进它们的检索，以及如何预测它们的定位和规模（尽管事实上<u>VLAD不存储任何空间信息</u>）**。

多个VLAD描述符（MultiVLAD）在三个标度上以常规3×3网格提取。提取了**14个VLAD描述符：最精细的九个（3×3），中等规模的四个（2×2）（每个瓦片由最精细的2×2个瓦片形成），一个覆盖整个图像**。在运行时，给定查询图像和覆盖查询对象的感兴趣区域（ROI），在ROI上计算单个VLAD并在数据库VLAD描述符之间进行匹配。为数据库中的图像分配等于其任何VLAD描述符与查询之间的最大相似度的分数。

如下所示，以**精细尺度计算VLAD描述符使得能够检索小对象，但代价是增加了存储（存储器）要求**。但是，每个图像有20个字节[8]，对于1亿个图像，每个图像有14个VLAD达到28 GB，这仍然是一个可管理的数据量，可以很容易地存储在商品服务器的主存储器中。

To assess the retrieval performance, additional ROI annotation is provided for the Oxford 5k dataset, as the original only specifies ROIs for the query images. Objects are deemed to be small if they occupy less than 300 × 300 pixels squared. Typical images in Oxford 5k are 1024 × 768, thus the threshold corresponds to the object occupying up to about 11% of an image. We measure the mean average precision for retrieving images containing these small objects using the standard Oxford 5k queries.

We compare to two baselines a single 128-D VLAD per image, and also a 14 × 128 = 1792-D VLAD. The latter is included for a fair comparison since MultiVLAD requires 14 times more storage. MultiVLAD achieves a mAP of 0.102, this outperforms the single 128-D VLAD descriptors, which only yield a mAP of 0.025, and also the 1792-D VLAD which obtains a mAP of 0.073, i.e. a 39.7% improvement. MultiVLAD consistently outperforms the 1792-D VLAD for thresholds smaller than 4002,and then is outperformed for objects occupying a significant portion of the image (more than 20% of it).

为了评估检索性能，为Oxford 5k数据集提供了额外的ROI注释，因为原始版本仅指定查询图像的ROI。如果物体占用小于300×300像素的平方，则认为物体很小。 Oxford 5k中的典型图像是1024×768，因此**阈值对应于占据图像的约11％的对象**。我们使用标准的Oxford 5k查询来测量检索包含这些小对象的图像的平均精度。

我们<u>将两个基线与每个图像的单个128-D VLAD进行比较，并且还比较14×128 = 1792-D VLAD。包含后者是为了公平比较，因为MultiVLAD需要14倍的存储空间</u>。 MultiVLAD达到0.102的mAP，这优于单个128-D VLAD描述符，其仅产生0.025的mAP，以及1792-D VLAD，其获得0.073的mAP，即39.7％的改善。对于小于4002的阈值，MultiVLAD始终优于1792-D VLAD，然后对于占据图像的重要部分的对象（超过20％），其性能优于对象。

**Implementation details**. The 3 × 3 grid is generated by splitting the horizontal and vertical axes into three equal parts. To account for potential featureless regions near image borders (e.g. the sky at the top of many images often contains no interest point detections), we adjust the outer boundary of the grid to the smallest bounding box which contains all interest points. All the multiple VLADs for an image can be computed efficiently through the use of an integral image of unnormalized VLADs.

**实施细节**。 通过将**水平轴和垂直轴分成三个相等的部分来生成3×3网格**。 为了考虑图像边界附近的潜在无特征区域（例如，许多图像顶部的天空通常不包含兴趣点检测），我们将网格的外边界调整到包含所有兴趣点的最小边界框。 通过使用非标准化VLAD的积分图像，可以有效地计算图像的所有多个VLAD。

**5.1. Fine object localization**

Given similarity scores between a query ROI and all the VLADs contained in the MultiVLAD of a result image, we show here how to obtain an estimate of the corresponding location within the result image. To motivate the method, consider figure 4 where, for each 200 × 200 subwindow of an image, VLAD similarities (to the VLAD of the target ROI) are compared to overlap (with the target ROI). The correlation is evident and we model this below using linear regression. The procedure is similar in spirit to the interpolation method of [17] for visual localization.

Implementation details. A similarity score vector s is computed between the query ROI VLAD and the VLADs corresponding to the image tiles of the result image’s MultiVLAD. We then seek an ROI in the result image whose overlap with the image tiles matches these similarity scores under a linear scaling. Here, overlap v(r) between an ROI r and an image tile is computed as the proportion of the image tile which is covered by the ROI. The best ROI, rbest,is determined by minimizing residuals as

给定查询ROI与结果图像的MultiVLAD中包含的所有VLAD之间的相似性得分，我们在此示出如何获得结果图像内的对应位置的估计。为了激励该方法，考虑图4，其中，对于图像的每个200×200子窗口，VLAD相似性（到目标ROI的VLAD）被比较以重叠（与目标ROI）。相关性很明显，我们使用线性回归对此进行建模。该过程在精神上类似于[17]的用于视觉定位的插值方法。

实施细节。在查询ROI VLAD和对应于结果图像的MultiVLAD的图像瓦片的VLAD之间计算相似性得分矢量s。然后，我们在结果图像中寻找ROI，其与图像块的重叠在线性缩放下匹配这些相似性得分。这里，ROI r和图像区块之间的重叠v（r）被计算为ROI覆盖的图像区块的比例。最好的ROI，rbest，通过最小化残差来确定

formula[2]

where any negative similarities are clipped to zero. Regressed overlap scores mimic the similarity scores very well, as shown by small residuals in figure 4d and 4e.

Note that given overlap scores v(r), which are easily computed for any ROI r, the inner minimization in (2) can be solved optimally using a closed form solution, as it is a simple least squares problem: the value of λ which minimizes the expression for a given r is 

To solve the full minimization problem we perform a brute force search in a discretized space of all possible rectangular ROIs. The discretized space is constructed out of all rectangles whose corners coincide with a very fine (30 by 30) regular grid overlaid on the image, i.e.there are31 distinct values considered for each of x and y coordinates. The number of all possible rectangles with non-zero area is  which amounts to 216k. 

The search procedure is very efficient as least squaresfitting is performed with simple 14-D scalar product computations, and the entire process takes 14 ms per image on a single core 3 GHz processor.

其中任何负面相似性被限制为零。回归重叠分数非常好地模拟相似性得分，如图4d和4e中的小残差所示。

注意，给定的重叠得分v（r），它们很容易为任何ROI r计算，（2）中的内部最小化可以使用闭式解决方案进行最优解决，因为它是一个简单的最小二乘问题：λ的值最小化给定r的表达式

为了解决完全最小化问题，我们在所有可能的矩形ROI的离散空间中执行强力搜索。离散空间由所有矩形构成，所述矩形的角与覆盖在图像上的非常精细（30×30）的规则网格重合，即对于x和y坐标中的每一个考虑31个不同的值。具有非零区域的所有可能矩形的数量等于216k。

搜索过程非常有效，因为使用简单的14-D标量积计算执行最小二乘拟合，并且整个过程在单核3 GHz处理器上每个映像需要14 ms。

**Localization accuracy.** To evaluate the localization quality the ground truth and estimated object positions and scales are compared in terms of the overlap score (i.e. the ratio between the intersection and union areas of the two ROIs), on the Oxford 5k dataset. In an analogous manner to computing mean average precision (mAP) scores for retrieval performance evaluation, for the purpose of localization evaluation the average overlap score is computed for each query, and averaged across queries to obtain the mean average overlap score. 

For the region descriptors we use MultiVLAD descriptors with center adaptation and intra-normalization, with multiple vocabularies trained on Paris and projected down to 128-D. This setup yields a mAP of 0.518 on Oxford 5k. 

The fine localization method is compared to two baselines: greedy and whole image.The whole image baseline returns the ROI placed over the entire image, thus always falling back to the “safe choice” and producing a non-zero overlap score. For the greedy baseline, the MultiVLAD retrieval system returns the most similar tile to the query in terms of similarity of their VLAD descriptors.

The mean average overlap scores for the three systems are 0.342, 0.369 and 0.429 for the whole image, greedy and fine respectively; the fine method improves the two baselines by 25% and 16%. Furthermore, we also measure the mean average number of times that the center of the estimated ROI is inside the ground truth ROI, and the fine method again significantly outperforms others by achieving a score of 0.897, which is a 28% and 8% improvement over whole image and greedy, respectively. Figure 5 shows a qualitative comparison of fine and greedy localization.

**定位精度。**为了评估定位质量，在Oxford 5k数据集上，根据重叠分数（即两个ROI的交叉点和联合区域之间的比率）比较地面实况和估计的物体位置和尺度。 。以类似于计算用于检索性能评估的平均精度（mAP）分数的方式，出于定位评估的目的，针对每个查询计算平均重叠分数，并且在查询之间平均以获得平均重叠分数。

对于区域描述符，我们使用具有中心自适应和内部归一化的MultiVLAD描述符，其中多个词汇表在巴黎训练并且投影到128-D。此设置在Oxford 5k上产生0.518的mAP。

将精细定位方法与两个基线进行比较：贪婪和整个图像。整个图像基线返回放置在整个图像上的ROI，因此总是回落到“安全选择”并产生非零重叠分数。对于贪婪的基线，MultiVLAD检索系统根据其VLAD描述符的相似性向查询返回最相似的区块。

三个系统的平均重叠分数分别为0.342,0.369和0.429，分别为贪婪和精细;精细方法将两个基线提高了25％和16％。此外，我们还测量了估计投资回报率中心在地面实况投资回报率中的平均平均次数，而精细方法再次显着优于其他人，得分为0.897，比28％和8％有所改善。整个形象和贪婪，分别。图5显示了精细和贪婪定位的定性比较。

#### VI. Results and discussion

In the following sections we compare our two improvements of the VLAD descriptor, namely cluster center adaptation and intra-normalization, with the state-of-the-art. First, the retrieval performance of the full size VLAD descriptors is evaluated, followed by tests on more compact descriptors obtained using dimensionality reduction, and then the variation in performance using vocabularies trained on different datasets is evaluated. Finally, we report on large scale experiments with the small descriptors. For all these tests we used RootSIFT descriptors clustered into k = 256 coarse clusters, and the vocabularies were trained on Paris and Flickr60k for Oxford 5k(+100k) and Holidays(+Flickr1M), respectively. 

在接下来的部分中，我们将VLAD描述符的两个改进，即集群中心自适应和内部归一化与现有技术进行比较。 首先，评估全尺寸VLAD描述符的检索性能，然后测试使用维数减少获得的更紧凑的描述符，然后评估使用在不同数据集上训练的词汇表的性能变化。 最后，我们报告了使用小描述符的大规模实验。 对于所有这些测试，我们使用聚类为k = 256个粗簇的RootSIFT描述符，并且分别针对Oxford 5k（+ 100k）和Holidays（+ Flickr1M）在Paris和Flickr60k上训练词汇。

**Full size VLAD descriptors.** Table 1 shows the performance of our method against the current state-of-the-art for descriptors of medium dimensionality (20k-D to 30k-D). Cluster center adaptation followed by intra-normalization outperforms all previous methods. For the Holidays dataset we outperform the best method (improved Fisher vectors [12]) by 3.2% on average and 4.3% in the best case, and for Oxford 5k we achieve an improvement of 4.3% and 4.9% in the average and best cases, respectively.

**Small image descriptors (128-D)**. We employ the state-ofthe-art method of [5] (Multivoc) which uses multiple vocabularies to obtain multiple VLAD (with SSR) descriptions of one image, and then perform dimensionality reduction, using PCA, and whitening to produce very small image descriptors (128-D). We mimic the experimental setup of [5], and learn the vocabulary and PCA on Paris 6k for the Oxford 5k tests. For the Holidays tests they do not specify which set of 10k Flickr images are used for learning the PCA. We use the last 10k from the Flickr1M [6] dataset. 

As can be seen from table 2, our methods outperform all current state-of-the-art methods. For Oxford 5k the improvement is 5.4%, while for Holidays it is 1.8%.

**全尺寸VLAD描述符。**表1显示了我们的方法针对中等维度描述符（20k-D到30k-D）的当前最新技术的性能。群集中心自适应后内部归一化优于所有先前的方法。对于假日数据集，我们的表现优于最佳方法（改进的Fisher矢量[12]）平均为3.2％，最佳情况为4.3％;对于牛津5k，我们分别在平均和最佳情况下的表现优于4.3％和4.9％  。

**小图像描述符（128-D）**。我们采用[5]（Multivoc）的最先进方法，它使用多个词汇表来获得一个图像的多个VLAD（带有SSR）描述，然后使用PCA执行降维，并使用白化来生成非常小的图像描述符（128-d）。我们模仿[5]的实验设置，并在Oxford 6k测试中学习巴黎6k的词汇和PCA。对于假期测试，他们没有指定使用哪组10k Flickr图像来学习PCA。我们使用Flickr1M [6]数据集中的最后10k。

从表2中可以看出，我们的方法优于所有当前最先进的方法。牛津5k的改善率为5.4％，而假期则为1.8％。

**Effect of using vocabularies trained on different datasets.** In order to assess how the retrieval performance varies when using different vocabularies, we measure the proportion of the ideal mAP (i.e. when the vocabulary is built on the benchmark dataset itself) achieved for each of the methods. 

First, we report results on Oxford 5k using full size VLADs in table 3. The baselines (VLAD and VLAD+SSR) perform very badly when an inappropriate (Flickr60k) vocabulary is used achieving only 68% of the ideal performance for the best baseline (VLAD+SSR). Using adapt+innorm, apart from improving mAP in general for all vocabularies, brings this score up to 86%. A similar trend is observed for the Holidays benchmark as well (see figure 2). 

We next report results for 128-D descriptors where, again, in all cases Multivoc [5] is used with PCA to perform dimensionality reduction and whitening. In addition to the residual problems caused by an inconsistent vocabulary, there is also the extra problem that the PCA is learnt on a different dataset. Using the Flickr60k vocabulary with adapt+innorm for Oxford 5k achieves 59% of the ideal performance, which is much worse than the 86% obtained with full size vectors above. Despite the diminished performance, adapt+innorm still outperforms the best baseline (VLAD+SSR) by 4%. A direction of future research is to investigate how to alleviate the influence of the inappropriate PCA training set, and improve the relative performance for small dimensional VLAD descriptors as well. 

**使用在不同数据集上训练的词汇表的效果。**为了评估在使用不同词汇表时检索性能如何变化，我们测量理想mAP的比例（即当词汇表建立在基准数据集本身时）每种方法。

首先，我们使用表3中的全尺寸VLAD报告Oxford 5k的结果。当使用不合适的（Flickr60k）词汇表时，基线（VLAD和VLAD + SSR）表现非常糟糕，仅达到最佳基线的理想性能的68％（ VLAD + SSR）。使用adapt + innorm，除了提高所有词汇表的mAP外，还可以将这一分数提高到86％。观察假期基准也有类似的趋势（见图2）。

我们接下来报告128-D描述符的结果，其中，在所有情况下，Multivoc [5]与PCA一起用于执行降维和白化。除了由不一致的词汇引起的残留问题之外，还存在在不同数据集上学习PCA的额外问题。使用适用于牛津5k的适应+ innorm的Flickr60k词汇表达到理想性能的59％，这比使用上述全尺寸矢量获得的86％差得多。尽管性能下降，但适应+ innorm仍然优于最佳基线（VLAD + SSR）4％。<u>未来研究的方向是研究如何减轻不适当的PCA训练集的影响，并提高小维VLAD描述符的相对性能。</u>

**Large scale retrieval.** With datasets of up to 1 million images and compact image descriptors (128-D) it is still possible to perform exhaustive nearest neighbor search. For example, in [5] exhaustive search is performed on 1 million 128-D dimensional vectors reporting 6 ms per query on a 12 core 3 GHz machine. Scaling to more than 1 million images is certainly possible using efficient approximate nearest neighbor methods.

The same 128-D descriptors (adapt+innorm VLADs reduced to 128-D using Multivoc) are used as described above. On Oxford 105k we achieve a mAP of 0.374, which is a 5.6% improvement over the best baseline, being (our reimplementation of) Multivoc VLAD+SSR. There are no previously reported results on compact image descriptors for this dataset to compare to. On Holidays+Flickr1M, adapt+innorm yields 0.378 compared to the 0.370 of Multivoc VLAD+SSR; while the best previously reported mAP for this dataset is 0.370 (using VLAD+SSR with full size VLAD and approximate nearest neighbor search [9]). Thus, we set the new state-of-the-art on both datasets here.

**大规模检索。**使用多达100万个图像和紧凑图像描述符（128-D）的数据集，仍然可以执行详尽的最近邻搜索。例如，在[5]中，在12核3 GHz机器上对每个查询报告6毫秒的100万个128维向量进行穷举搜索。使用有效的近似最近邻方法，当然可以缩放到超过100万个图像。

如上所述使用相同的128-D描述符（使用Multivoc将+ innorm VLAD减少到128-D）。在Oxford 105k上，我们实现了0.374的mAP，比最佳基线提高了5.6％，是（我们重新实现）Multivoc VLAD + SSR。此数据集的紧凑图像描述符之前没有报告结果可供比较。在假日+ Flickr1M上，与Multivoc VLAD + SSR的0.370相比，适应+ innorm产生0.378;而此前报告的该数据集的最佳mAP为0.370（使用VLAD + SSR，全尺寸VLAD和近似最近邻搜索[9]）。因此，我们在这两个数据集上设置了新的最新技术.

#### VII. Conclusions and recommendations 

We have presented three methods which improve standard VLAD descriptors over various aspects, namely cluster center adaptation, intra-normalization and MultiVLAD. 

Cluster center adaptation is a useful method for large scale retrieval tasks where image databases grow with time as content gets added. It somewhat alleviates the influence of using a bad visual vocabulary, without the need of recomputing or storing all local descriptors. 

Intra-normalization was introduced in order to fully suppress bursty visual elements and provide a better measure of similarity between VLAD descriptors. It was shown to be the best VLAD normalization scheme. However, we recommend intra-normalization always be used in conjunction with a good visual vocabulary or with center adaptation (as intra-normalization is sometimes outperformed by SSR when inconsistent clusters are used and no center adaptation is performed). Although it is outside the scope of this paper, intra-normalized VLAD also improves image classification performance over the original VLAD formulation.

我们提出了三种方法，它们在各个方面改进了标准VLAD描述符，即**聚类中心自适应，内部归一化和MultiVLAD**。

群集中心自适应是大规模检索任务的有用方法，其中图像数据库随着内容的增加而随着时间而增长。它有点减轻了使用不良视觉词汇的影响，而无需重新计算或存储所有本地描述符。

引入归一化以便完全抑制突发的视觉元素并提供VLAD描述符之间的相似性的更好测量。它被证明是最好的VLAD归一化方案。但是，我们建议帧内归一化始终与良好的视觉词汇或中心自适应一起使用（因为当使用不一致的簇并且不执行中心自适应时，帧内归一化有时优于SSR）。虽然它超出了本文的范围，但是内部归一化的VLAD也提高了原始VLAD配方的图像分类性能。



### NetVLAD文章内容

https://blog.csdn.net/qq_32417287/article/details/80102466

#### 公式声明参数

公式一:vlad特性

公式2:softmax函数增加可微

公式3:

| 参量                   | 定义                                                         | 备注                                                        |
| ---------------------- | ------------------------------------------------------------ | ----------------------------------------------------------- |
| {xi}                   | local image descriptors                                      |                                                             |
| K                      | cluster centres numbers                                      |                                                             |
| c_k *                  | cluster centres (“visual words”)                             |                                                             |
| a_k(xi)                | the weight of descriptor xi to cluster ck proportional to their proximity, | 0到1之间,for the closest cluster would be 1 and 0 otherwise |
| wk =2αc *              |                                                              | softmax函数拆开表示                                         |
| $ b_k = −α {c_k}^2 $ * |                                                              | softmax函数拆开表示                                         |
| σk(z)                  |                                                              | 借助该数值最终确定a_k                                       |
| q                      | given training query                                         |                                                             |
| $p_{i}^{q}$            | potential positives                                          |                                                             |
| $ n_{j}^{q} $          | definite negatives                                           |                                                             |
|                        |                                                              |                                                             |
|                        |                                                              |                                                             |
|                        |                                                              |                                                             |
|                        |                                                              |                                                             |
|                        |                                                              |                                                             |

#### Abstract

We tackle the problem ofl arge scale visual place recognition, where the task is to quickly and accurately recognize the location of a given query photograph. We present the following three principal contributions. First, we develop a convolutional neural network (CNN) architecture that is trainable in an end-to-end manner directly for the place recognition task. The main component of this architecture, NetVLAD, is a new generalized VLAD layer, inspired by the “Vector of Locally Aggregated Descriptors” image representation commonly used in image retrieval. The layer is readily pluggable into any CNN architecture and amenable to training via backpropagation. Second, we develop a training procedure, based on a new weakly supervised ranking loss, to learn parameters of the architecture in an end-to-end manner from images depicting the same places over time downloaded from Google Street View Time Machine. Finally, we show that the proposed architecture significantly outperforms non-learnt image representations and off-the-shelf CNN descriptors on two challenging place recognition benchmarks, and improves over current stateof-the-art compact image representations on standard image retrieval benchmarks.

我们解决了大规模视觉位置识别的问题，其任务是快速准确地识别给定查询照片的位置。我们提出以下三个主要贡献。首先，我们开发了一种卷积神经网络（CNN）架构，该架构可以**端到端的方式直接用于场所识别任务**。这个体系结构的主要组成部分**NetVLAD是一个新的通用VLAD层**，受到图像检索中常用的“局部聚合描述符向量”图像表示的启发。该层可以很容易地插入任何CNN架构中，并且可以通过反向传播进行训练。其次，我们开发了一个基于新的弱监督排名损失的培训程序，从描绘从**谷歌街景时间机器下载的相同位置的图像**以端到端的方式学习架构的参数。最后，我们表明，所提出的架构在两个具有挑战性的场所识别基准上明显优于非学习图像表示和现成的CNN描述符，并改进了标准图像检索基准上的当前最先进的**紧凑图像表示**。

#### I. Introduction

*Figure 1. Our trained NetVLAD descriptor correctly recognizes the location (b) of the query photograph (a) despite the large amount of clutter (people, cars), changes in viewpoint and completely different illumination (night vs daytime). Please see the appendix [2] for more examples.*

图1.我们训练有素的NetVLAD描述符正确识别查询照片的位置（b）（a）尽管杂乱（人，车），视点变化和完全不同的照明（夜晚与白天）有很多混乱。 有关更多示例，请参阅附录[2]。

Visual place recognition has received a significant amount of attention in the past years both in computer vision [5, 10, 11, 24, 35, 62, 63, 64, 65, 79, 80] and robotics communities [16, 17, 44, 46, 74] motivated by, e.g., applications in autonomous driving [46], augmented reality [47] or geo-localizing archival imagery [6]. 

The place recognition problem, however, still remains extremely challenging. How can we recognize the same street-corner in the entire city or on the scale of the entire country despite the fact it can be captured in different illuminations or change its appearance over time? The fundamental scientific question is what is the appropriate representation of a place that is rich enough to distinguish similarly looking places yet compact to represent entire cities or countries. 

The place recognition problem has been traditionally cast as an instance retrieval task, where the query image location is estimated using the locations of the most visually similar images obtained by querying a large geotagged database [5, 11, 35, 65, 79, 80]. **Each database image is represented using local invariant features [82] such as SIFT [43] that are aggregated into a single vector representation for the entire image such as bag-of-visualwords [53, 73], VLAD [4, 29] or Fisher vector [31, 52].** The resulting representation is then usually compressed and efficiently indexed [28, 73]. The image database can be further augmented by 3D structure that enables recovery of accurate camera pose [40, 62, 63].

In the last few years convolutional neural networks (CNNs) [38, 39] have emerged as powerful image representations for various category-level recognition tasks such as object classification [37, 49, 72, 76], scene recognition [89] or object detection [22]. The basic principles of CNNs are known from 80’s [38, 39] and the recent successes are a combination of advances in GPU-based computation power together with large labelled image datasets [37]. While it has been shown that the trained representations are, to some extent, transferable between recognition tasks [20, 22, 49, 68, 87], a direct application of CNN representations trained for object classification [37] as black-box descriptor extractors has so far yielded limited improvements in performance on instance-level recognition tasks [7, 8, 23, 60, 61]. 

In this work we investigate whether this gap in performance can be bridged by CNN representations developed and trained directly for place recognition. This requires addressing the following three main challenges. First, what is a good CNN architecture for place recognition? Second, how to gather sufficient amount of annotated data for the training? Third, how can we train the developed architecture in an end-toend manner tailored for the place recognition task? To address these challenges we bring the following three innovations.

在过去几年中，视觉位置识别在计算机视觉[5,10,11,24,35,62,63,64,65,79,80]和机器人社区[16,17,44]中受到了极大的关注，46,74]，例如，在自动驾驶[46]，增强现实[47]或地理定位档案图像[6]中的应用。

然而，地点识别问题仍然极具挑战性。我们怎样才能识别整个城市或整个国家的街道角落，尽管它可以在不同的照明中被捕获或随着时间的推移而改变其外观？一个基本的科学问题是，一个地方的适当表现是什么，它足够丰富，可以区分同样看起来的地方，但又很紧凑，代表整个城市或国家。

地点识别问题传统上被视为实例检索任务，其中使用通过查询大型地理标记数据库获得的视觉上最相似的图像的位置来估计查询图像位置[5,11,35,65,79,80]。 **每个数据库图像使用局部不变特征[82]表示，例如SIFT [43]，它们聚合成整个图像的单个矢量表示，如视觉袋[53,73]，VLAD [4,29]或费舍尔矢量[31,52]。**然后通常压缩并有效地索引所得到的表示[28,73]。图像数据库可以通过3D结构进一步增强，这使得能够恢复准确的相机姿势[40,62,63]。

在过去几年中，卷积神经网络（CNNs）[38,39]已成为各种类别级别识别任务的强大图像表示，如对象分类[37,49,72,76]，场景识别[89]或对象检测[22]。 CNN的基本原理在80年代[38,39]已知，最近的成功是基于GPU的计算能力与大型标记图像数据集的进步相结合[37]。虽然已经证明训练的表示在某种程度上可以在识别任务之间转移[20,22,49,68,87]，但是将对象分类训练的CNN表示直接应用[37]作为黑盒描述符提取器到目前为止，在实例级别的识别任务中，性能得到了有限的改进[7,8,23,60,61]。

在这项工作中，我们调查是否可以通过CNN表示直接开发和训练地点识别来弥补性能差距。这需要解决以下三个主要挑战。

首先，什么是用于场所识别的良好CNN架构？

第二，如何为培训收集足够数量的注释数据？

第三，我们如何以端到端的方式培训开发的架构，以便为场所识别任务量身定制？

为了应对这些挑战，我们提出了以下三项创新。

First, building on the lessons learnt from the current well performing hand-engineered object retrieval and place recognition pipelines [3, 4, 25, 79] we develop a convolutional neural network architecture for place recognition that aggregates mid-level (conv5) convolutional features extracted from the entire image into a compact single vector representation amenable to efficient indexing. To achieve this, we design a new trainable generalized VLAD layer, NetVLAD, inspired by the Vector of Locally Aggregated Descriptors (VLAD) representation [29] that has shown excellent performance in image retrieval and place recognition. The layer is readily pluggable into any CNN architecture and amenable to training via backpropagation. The resulting aggregated representation is then compressed using Principal Component Analysis (PCA) to obtain the final compact descriptor of the image.

Second, to train the architecture for place recognition, we gather a large dataset of multiple panoramic images depicting the same place from different viewpoints over time from the Google Street View Time Machine. Such data is available for vast areas of the world, but provides only weak form of supervision: we know the two panoramas are captured at approximately similar positions based on their (noisy) GPS but we don’t know which parts of the panoramas depict the same parts of the scene.

Third, we develop a learning procedure for place recognition that learns parameters of the architecture in an endto-end manner tailored for the place recognition task from the weakly labelled Time Machine imagery. The resulting representation is robust to changes in viewpoint and lighting conditions, while simultaneously learns to focus on the relevant parts of the image such as the building fac¸ades and the skyline, while ignoring confusing elements such as cars and people that may occur at many different places.

We show that the proposed architecture significantly outperforms non-learnt image representations and off-theshelf CNN descriptors on two challenging place recognition benchmarks, and improves over current state-of-the-art compact image representations on standard image retrieval benchmarks.

首先，基于从当前表现良好的手工设计对象检索和位置识别管道中获得的经验教训[3,4,25,79]，我们开发了一种用于位置识别的卷积神经网络架构，该架构聚合了中级（conv5）卷积特征从整个图像中提取出一个紧凑的单一矢量表示，适合于有效的索引。为实现这一目标，我们设计了一个新的可训练的广义VLAD层，NetVLAD，受到局部聚合描述符向量（VLAD）表示[29]的启发，在图像检索和位置识别方面表现出色。该层可以很容易地插入任何CNN架构中，并且可以通过反向传播进行训练。然后使用主成分分析（PCA）压缩得到的聚合表示，以获得图像的最终紧凑描述符。

其次，为了训练地点识别的架构，我们从Google街景时间机器收集了多个全景图像的大型数据集，描绘了不同视点中不同视点的相同位置。这些数据可用于世界各地，但仅提供弱监督形式：我们知道两个全景图基于其（嘈杂的）GPS在大致相似的位置捕获但我们不知道全景图的哪些部分描绘了场景的相同部分。

第三，我们开发了一种用于地点识别的学习过程，该过程以弱端标记的时间机器图像为地点识别任务定制的端到端方式学习该体系结构的参数。由此产生的表示对于视点和光照条件的变化是稳健的，同时学会专注于图像的相关部分，例如建筑物外观和天际线，同时忽略可能出现在许多地方的混乱元素，例如汽车和人不同的地方。

我们表明，所提出的体系结构在两个具有挑战性的地点识别基准上明显优于非学习图像表示和非空中CNN描述符，并且改进了标准图像检索基准上的当前最先进的紧凑图像表示。

1.1. Related work 

While there have been many improvements in designing better image retrieval [3, 4, 12, 13, 18, 25, 26, 27, 29, 32, 48, 51, 52, 53, 54, 70, 77, 78, 81] and place recognition [5, 10, 11, 16, 17, 24, 35, 44, 46, 62, 63, 64, 74, 79, 80] systems, not many works have performed learning for these tasks. All relevant learning-based approaches fall into one or both of the following two categories: (i) learning for an auxiliary task (e.g. some form of distinctiveness of local features [5, 16, 30, 35, 58, 59, 88]), and (ii) learning on top of shallow hand-engineered descriptors that cannot be finetuned for the target task [3, 10, 24, 35, 57]. Both of these are in spirit opposite to the core idea behind deep learning that has provided a major boost in performance in various recognition tasks: end-to-end learning. We will indeed show in section 5.2 that training representations directly for the endtask, place recognition, is crucial for obtaining good performance.

Numerous works concentrate on learning better local descriptors or metrics to compare them [45, 48, 50, 55, 56, 69, 70, 86], but even though some of them show results on image retrieval, the descriptors are learnt on the task of matching local image patches, and not directly with image retrieval in mind. Some of them also make use of handengineered features to bootstrap the learning, i.e. to provide noisy training data [45, 48, 50, 55, 70]. 

Several works have investigated using CNN-based features for image retrieval. These include treating activations from certain layers directly as descriptors by concatenating them [9, 60], or by pooling [7, 8, 23]. However, none of these works actually train the CNNs for the task at hand, but use CNNs as black-box descriptor extractors. One exception is the work of Babenko et al.[9] in which the network is fine-tuned on an auxiliary task of classifying 700 landmarks. However, again the network is not trained directly on the target retrieval task. 

Finally, recently [34] and [41] performed end-to-end learning for different but related tasks of ground-to-aerial matching [41] and camera pose estimation [34].	

虽然在设计更好的图像检索方面有许多改进[3,4,12,13,18,25,26,27,29,32,48,51,52,53,54,70,77,78,81]和地点识别[5,10,11,16,17,24,35,44,46,62,63,64,74,79,80]系统，没有多少工作已经为这些任务进行了学习。所有相关的基于学习的方法都属于以下两个类别中的一个或两个：（i）学习辅助任务（例如某种形式的局部特征的独特性[5,16,30,35,58,59,88]） ，以及（ii）在浅层手工设计描述符之上进行学习，这些描述符无法针对目标任务进行微调[3,10,24,35,57]。这两者都与深度学习背后的核心理念相反，后者在各种识别任务中提供了性能的重大提升：端到端学习。我们将在5.2节中确实表明，直接针对结束任务，地点识别的培训表示对于获得良好的表现至关重要。

许多作品专注于学习更好的局部描述符或度量来比较它们[45,48,50,55,56,69,70,86]，但即使其中一些显示图像检索的结果，描述符也是在任务上学习的匹配本地图像补丁，而不是直接考虑图像检索。他们中的一些人还利用handengineered功能来引导学习，即提供嘈杂的训练数据[45,48,50,55,70]。

一些作品已经研究了使用基于CNN的特征进行图像检索。这些包括将某些层的激活直接作为描述符通过连接它们[9,60]或通过合并[7,8,23]来处理。然而，这些工作中没有一个实际上训练CNN用于手头的任务，而是使用CNN作为黑盒描述符提取器。一个例外是Babenko等人的工作[9]其中网络在对700个地标进行分类的辅助任务上进行微调。但是，网络不再直接在目标检索任务上进行训练。

最后，最近[34]和[41]对地对空匹配[41]和相机姿态估计[34]的不同但相关的任务进行了端到端学习。

#### II. Method overview Building

Building on the success of current place recognition systems (e.g.[5, 11, 35, 62, 63, 64, 65, 79, 80]), we cast place recognition as image retrieval. The query image with unknown location is used to visually search a large geotagged image database, and the locations of top ranked images are used as suggestions for the location of the query. This is generally done by designing a function f which acts as the “image representation extractor”, such that given an image Ii it produces a fixed size vector f(Ii). The function is used which can be done offline, and to extract the query image to extract the representations for the entire database {Ii}, representation f(q), done online. At test time, the visual search is performed by finding the nearest database image to the query, either exactly or through fast approximate nearest neighbour search, by sorting images based on the Euclidean distance d(q, Ii) between f(q) and f(Ii).

While previous works have mainly used handengineered image representations (e.g. f(I) corresponds to extracting SIFT descriptors [43], followed by pooling into a bag-of-words vector [73] or a VLAD vector [29]), here we propose to learn the representation f(I) in an end-to-end manner, directly optimized for the task of place recognition. The representation is parametrized with a set of parameters θ and we emphasize this fact by referring to it as fθ(I). It follows that the Euclidean distance dθ(Ii,Ij)= ?fθ(Ii) − fθ(Ij)? also depends on the same parameters. An alternative setup would be to learn the distance function itself, but here we choose to fix the distance function to be Euclidean distance, and to pose our problem as the search for the explicit feature map fθ which works well under the Euclidean distance.

In section 3 we describe the proposed representation fθ based on a new deep convolutional neural network architecture inspired by the compact aggregated image descriptors for instance retrieval. In section 4 we describe a method to learn the parameters θ of the network in an end-to-end manner using weakly supervised training data from the Google Street View Time Machine.

在当前场所识别系统成功的基础上（例如[5,11,35,62,63,64,65,79,80]），我们将场所识别作为图像检索。具有未知位置的查询图像用于在视觉上搜索大的地理标记图像数据库，并且排名最高的图像的位置用作查询位置的建议。这通常通过设计充当“图像表示提取器”的函数f来完成，使得给定图像Ii它产生固定大小的矢量f（Ii）。使用该功能可以离线完成，并提取查询图像以提取在线完成的整个数据库{Ii}，表示f（q）的表示。在测试时，通过基于f（q）和f之间的欧几里德距离d（q，Ii）对图像进行排序，通过精确地或通过快速近似最近邻搜索找到最接近查询的数据库图像来执行视觉搜索。 （二）。

虽然以前的作品主要使用了handengineered图像表示（例如f（I）对应于提取SIFT描述符[43]，然后汇集成一个词袋矢量[73]或VLAD矢量[29]），这里我们建议以端到端的方式学习表示f（I），直接针对地点识别任务进行优化。该表示用一组参数θ进行参数化，我们通过将其称为fθ（I）来强调这一事实。由此得出欧几里德距离dθ（Ii，Ij）=Δfθ（Ii）-fθ（Ij）≤也取决于相同的参数。另一种设置是学习距离函数本身，但是在这里我们选择将距离函数固定为欧几里德距离，并将我们的问题作为搜索在欧几里德距离下工作良好的显式特征映射fθ。

在第3节中，我们描述了基于新的深度卷积神经网络架构的所提出的表示f0，该架构受到紧凑的聚合图像描述符的启发，例如检索。在第4节中，我们描述了一种使用来自Google街景时间机器的弱监督训练数据以端到端方式学习网络参数θ的方法。

#### III. Deep architecture for place recognition

This section describes the proposed CNN architecture fθ, guided by the best practises from the image retrieval community. Most image retrieval pipelines are based on (i) extracting local descriptors, which are then (ii) pooled in an orderless manner. The motivation behind this choice is that the procedure provides significant robustness to translation and partial occlusion. Robustness to lighting and viewpoint changes is provided by the descriptors themselves, and scale invariance is ensured through extracting descriptors at multiple scales.

In order to learn the representation end-to-end, we design a CNN architecture that mimics this standard retrieval pipeline in an unified and principled manner with differentiable modules. For step (i), we crop the CNN at the last convolutional layer and view it as a dense descriptor extractor. This has been observed to work well for instance retrieval [7, 8, 61] and texture recognition [14]. Namely, map which can be considered as a set of D-dimensional dethe output of the last convolutional layer is a H ×W × D scriptors extracted at H × W spatial locations. For step (ii) we design a new pooling layer inspired by the Vector of Locally Aggregated Descriptors (VLAD) [29] that pools extracted descriptors into a fixed image representation and its parameters are learnable via back-propagation. We call this new pooling layer “NetVLAD” layer and describe it in the next section.

本节描述了建议的CNN架构fθ，由图像检索社区的最佳实践指导。大多数图像检索管道基于（i）提取局部描述符，然后（ii）以无序方式合并。这种选择背后的动机是该程序为翻译和部分遮挡提供了显着的稳健性。描述符本身提供了对照明和视点变化的稳健性，并且通过在多个尺度上提取描述符来确保尺度不变性。

为了学习端到端的表示，我们设计了一个CNN架构，该架构以统一且有原则的方式模拟这个标准检索流水线，并具有可区分的模块。对于步骤（i），**我们在最后一个卷积层裁剪CNN并将其视为密集描述符提取器。**已经观察到这适用于例如检索[7,8,61]和纹理识别[14]。即，可以被认为是最后一个卷积层的输出的D维集合的映射是在H×W空间位置处提取的H×W×D脚本。对于步骤（ii），**我们设计了一个新的池化层，其灵感来自局部聚合描述符向量（VLAD）[29]，它将提取的描述符汇集成固定的图像表示，并且其参数可通过反向传播来学习。**我们将这个新的池化层称为“NetVLAD”层，并在下一节中对其进行描述。

3.1. NetVLAD:A Generalized VLAD layer

Vector of Locally Aggregated Descriptors (VLAD) [29] is a popular descriptor pooling method for both instance level retrieval [29] and image classification [23]. It captures information about the statistics of local descriptors aggregated over the image. Whereas bag-of-visual-words [15, 73] aggregation keeps counts of visual words, VLAD stores the sum of residuals (difference vector between the descriptor and its corresponding cluster centre) for each visual word. 

Formally, given N D-dimensional local image descrip\tors {xi} as input, and K cluster centres (“visual words”) write V as a K×D matrix, but this matrix is converted into sentation V is K×D-dimensional. For convenience we will {ck} as VLAD parameters, the output VLAD image reprea vector and, after normalization, used as the image representation. The (j, k) element of V is computed as follows:

局部聚合描述符（VLAD）[29]的向量是用于实例级别检索[29]和图像分类[23]的流行描述符池方法。 它捕获有关在图像上聚合的局部描述符统计信息的信息。 尽管视觉词袋[15,73]聚合保持视觉词的计数，但VLAD存储每个视觉词的残差之和（描述符与其对应的聚类中心之间的差矢量）。

形式上，给定N D维局部图像描述\ tors {xi}作为输入，并且K个聚类中心（“视觉词”）将V写为K×D矩阵，但是该矩阵被转换为发送V是K×D-维。 为方便起见，我们将{ck}作为VLAD参数，输出VLAD图像表示矢量，并在归一化后用作图像表示。 V的（j，k）元素计算如下：

formula1

where xi(j) and ck(j) are the j-th dimensions of the i-th descriptor and k-th cluster centre, respectively. ak(xi) denotes the membership of the descriptor xi to k-th visual word, i.e.itis 1 if cluster ck is the closest cluster to descriptor xi and 0 otherwise. Intuitively, each D-dimensional column k ofV records the sum of residuals (xi −ck) of descriptors which are assigned to cluster ck. The matrix V is then L2-normalized column-wise (intra-normalization [4]), converted into a vector, and finally L2-normalized in its entirety [29].

In order to profit from years of wisdom produced in image retrieval, we propose to mimic VLAD in a CNN framework and design a trainable generalized VLAD layer, NetVLAD. The result is a powerful image representation trainable end-to-end on the target task (in our case place recognition). To construct a layer amenable to training via backpropagation, it is required that the layer’s operation is differentiable with respect to all its parameters and the input. Hence, the key challenge is to make the VLAD pooling differentiable, which we describe next. 

The source of discontinuities in VLAD is the hard assignment ak(xi) of descriptors xi to clusters centres ck.To make this operation differentiable, we replace it with soft assignment of descriptors to multiple clusters

其中xi（j）和ck（j）分别是第i个描述符和第k个聚类中心的第j维。 ak（xi）将描述符xi的成员资格记录为第k个视觉单词，即如果集群ck是最接近描述xi的集群，则为1，否则为0。直观地，V的每个D维列k记录分配给集群ck的描述符的残差（xi -ck）的总和。然后矩阵V按列标准化（归一化[4]内），转换为矢量，最后在其完整性中进行L2归一化[29]。

为了从图像检索中产生的多年智慧中获益，我们建议在CNN框架中模拟VLAD并设计可训练的通用VLAD层NetVLAD。结果是在目标任务（在我们的例子中为地点识别）上端到端训练的强大图像表示。要构建一个适合通过反向传播进行训练的层，需要该层的操作在其所有参数和输入方面是可微分的。因此，关键的挑战是使VLAD池可微分，我们接下来会描述。

VLAD中的不连续性源是描述符xi到聚类中心ck的硬分配ak（xi）。为了使该运算可微分，我们用多个聚类的描述符的软分配来代替它。

formula2

which assigns the weight of descriptor xi to cluster ck proportional to their proximity, but relative to proximities to other cluster centres. ¯ak(xi) ranges between 0 and 1, with the highest weight assigned to the closest cluster centre. α is a parameter (positive constant) that controls the decay of the response with the magnitude of the distance. Note that for α → +∞ this setup replicates the original VLAD exactly as ¯ak(xi) for the closest cluster would be 1 and 0 otherwise

By expanding the squares in (2), it is easy to see that the term xxx cancels between the numerator and the denominator resulting in a soft-assignment of the following form

formula3



Relations to other methods. Other works have proposed to pool CNN activations using VLAd or Fisher Vectors (FV) [14,231, but do not learn the VLAD/FV parameters nor the input descriptors. The most related method to ours is the one of Sydorov et al. [75), which proposes to learn Fv parameters jointly with an SVM for the end classification objective. However, in their work it is not possible to learn the input descriptors as they are hand-engineered(SIFT),while our VLAD layer is easily pluggable into any CNN architecture as it is amenable to backpropagation. "Fisher Networks"[71] stack Fisher Vector layers on top of each other, but the system is not trained end-to-end, only hand-crafte ted features are used. and the layers are trained greedily in a bottom-up fashion. Finally, our architecture is also relate ted to bilinear networks [42]. recently developed for a different task of fine-grained category-level recognition.

与其他方法的关系。 其他工作已提出使用VLAd或Fisher矢量（FV）[14,231]来汇集CNN激活，但不学习VLAD / FV参数和输入描述符。 与我们最相关的方法是Sydorov等人的方法。 [75]，它建议与SVM一起学习Fv参数，用于最终分类目标。 然而，在他们的工作中，不可能学习输入描述符，因为它们是手工设计（SIFT），而我们的VLAD层可以很容易地插入任何CNN架构，因为它适合于反向传播。 “Fisher网络”[71]将Fisher Vector层叠在一起，但系统没有经过端到端的训练，只使用了手工制作的功能。 并且这些层以自下而上的方式贪婪地训练。 最后，我们的架构也与双线性网络有关[42]。 最近开发用于细粒度类别级别识别的不同任务。

Max pooling (fmax). We also experiment with Maxspatial locations, thus producing a D-dimensional output pooling of the D-dimensional features across the H × W vector, which is then L2-normalized. Both of these operations can be implemented using standard layers in public CNN packages. This setup mirrors the method of [7, 61], but a crucial difference is that we will learn the representation (section 4) while [7, 60, 61] only use pretrained networks. Results will show (section 5.2) that simply using CNNs off-the-shelf [60] results in poor performance, and that training for the end-task is crucial. Additionally, VLAD will prove itself to be superior to the Max-pooling baseline.

（fmax）。 我们还试验了最大空间位置，从而在H×W向量上产生D维特征的D维输出汇集，然后进行L2归一化。 这两种操作都可以使用公共CNN包中的标准层来实现。 这种设置反映了[7,61]的方法，但一个关键的区别是我们将学习代表（第4节），而[7,60,61]只使用预训练的网络。 结果将显示（第5.2节）仅仅使用现成的CNN [60]会导致性能不佳，并且对最终任务的培训至关重要。 此外，VLAD将证明自己优于Max-pooling基线。

#### IV. Learning from Time Machine data

In the previous section we have designed a new CNN architecture as an image representation for place recognition. Here we describe how to learn its parameters in an end-toend manner for the place recognition task. The two main challenges are: (i) how to gather enough annotated training data and (ii) what is the appropriate loss for the place recognition task. To address theses issues, we will first show that it is possible to obtain large amounts of weakly labelled imagery depicting the same places over time from the Google Street View Time Machine. Second, we will design a new weakly supervised triplet ranking loss that can deal with the incomplete and noisy position annotations of the Street View Time Machine imagery. The details are below

Weak supervision from the Time Machine. We propose to exploit a new source of data – Google Street View Time Machine – which provides multiple street-level panoramic images taken at different times at close-by spatial locations on the map. As will be seen in section 5.2, this novel data source is precious for learning an image representation for place recognition. As shown in figure 4, the same locations are depicted at different times and seasons, providing the learning algorithm with crucial information it can use to discover which features are useful or distracting, and what changes should the image representation be invariant to, in order to achieve good place recognition performance

The downside of the Time Machine imagery is that it provides only incomplete and noisy supervision. Each Time Machine panorama comes with a GPS tag giving only its approximate location on the map, which can be used to identify close-by panoramas but does not provide correspondences between parts of the depicted scenes. In detail, as the test queries are perspective images from camera phones, each panorama is represented by a set of perspective images sampled evenly in different orientations and two elevation angles [11, 24, 35, 80]. Each perspective image is labelled with the GPS position of the source panorama. As a result, two geographically close perspective images do not necessarily depict the same objects since they could be facing different directions or occlusions could take place (e.g. the two images are around a corner from each other), etc. Therefore, for a given training query q, the GPS information can only be used as a source of (i) potential positives {pqimages that are geographically close to the query, and (ii) definite negatives {nqj}, i.e. images that are geographically far from the query.

**在上一节中，我们设计了一种新的CNN架构作为场所识别的图像表示。在这里，我们将描述如何以端到端的方式为场所识**别任务学习其参数。两个主要挑战是：<u>（i）如何收集足够的注释训练数据和（ii）地点识别任务的适当损失是什么。</u>为了解决这些问题，我们首先会证明，从Google街景时间机器中可以获得大量描绘相同位置的弱标签图像。其次，我们将设计一个新的弱监督三元组排名损失，可以处理街景时间机器图像的不完整和嘈杂的位置注释。详情如下

来自Time Machine的弱监督。我们建议利用新的数据源 谷歌街景时间机器 它提供在地图上靠近空间位置的不同时间拍摄的多个街道级全景图像。如将在5.2节中看到的，这种新颖的数据源对于学习用于位置识别的图像表示是宝贵的。如图4所示，在不同的时间和季节描绘相同的位置，为学习算法提供可用于发现哪些特征有用或分散注意力的关键信息，以及图像表示应该对哪些变化不变，以便实现良好的场所识别性能

Time Machine图像的缺点是它只提供不完整和嘈杂的监督。每个Time Machine全景图都带有一个GPS标签，仅在地图上给出其大致位置，可用于识别近景全景图，但不提供所描绘场景的各部分之间的对应关系。详细地，由于测试查询是来自照相手机的透视图像，因此每个全景图由在不同方位和两个仰角[11,24,35,80]均匀采样的一组透视图像表示。每个透视图像都标有源全景图的GPS位置。结果，两个地理上接近的透视图像不一定描绘相同的物体，因为它们可能面向不同的方向或者可能发生遮挡（例如，两个图像彼此相邻的角落），等等。因此，对于给定的训练查询q，GPS信息只能用作（i）潜在的正面信息（地理上接近查询的pqimages）和（ii）明确的负数{nqj}的源，即地理上远离查询的图像。

Weakly supervised triplet ranking loss. We wish to learn a representation fθ that will optimize place recognition performance. That is, for a given test query image q, the goal higher than all other far away images Ii in the database. In is to rank a database image Ii∗ from a close-by location other words, we wish the Euclidean distance dθ(q, I) between the query q and a close-by image Ii∗ to be smaller than the distance to far away images in the database Ii, i.e. dθ(q, Ii∗) <dθ(q, Ii), for all images Ii further than a certain distance from the query on the map. Next we show how this requirement can be translated into a ranking loss between training triplets {q, Ii∗,Ii}.

弱监督三元组排名损失。 我们希望学习一种能够优化地点识别性能的表示。 也就是说，对于给定的测试查询图像q，目标高于数据库中的所有其他远处图像Ii。 In是从邻近位置对数据库图像Ii *进行排序其他单词，我们希望查询q和近景图像Ii *之间的欧几里德距离dθ（q，I）小于到达距离的距离 数据库Ii中的远距离图像，即dθ（q，Ii *）<dθ（q，Ii），对于距离地图上的查询的某个距离的所有图像Ii。 接下来，我们将展示如何将此要求转换为训练三元组{q，Ii *，Ii}之间的排名损失。

From the Google Street View Time Machine data, we obtain a training dataset of tuples for each training query image q we have a set of potential


#### V. Experiments

In this section we describe the used datasets and evaluation methodology (section 5.1), and give quantitative (section 5.2) and qualitative (section 5.3) results to validate our approach. Finally, we also test the method on the standard image retrieval benchmarks (section 5.4).

在本节中，我们将描述使用的数据集和评估方法（第5.1节），并给出定量（第5.2节）和定性（第5.3节）结果以验证我们的方法。 最后，我们还在标准图像检索基准测试方法上进行测试（第5.4节）。

5.1. Datasets and evaluation methodology 

We report results on two publicly available datasets.

Pittsburgh (Pitts250k) [80] contains 250k database images downloaded from Google Street View and 24k test queries generated from Street View but taken at different times, years apart. We divide this dataset into three roughly equal parts for training, validation and testing, each containing around 83k database images and 8k queries, where the division was done geographically to ensure the sets contain independent images. To facilitate faster training, for some experiments, a smaller subset (Pitts30k) is used, containing 10k database images in each of the train/val(idation)/test sets, which are also geographically disjoint. 

Tokyo 24/7 [79] contains 76k database images and 315 query images taken using mobile phone cameras. This is an extremely challenging dataset where the queries were taken at daytime, sunset and night, while the database images were only taken at daytime as they originate from Google Street View as described above. To form the train/val sets we collected additional Google Street View panoramas of Tokyo using the Time Machine feature, and name this set TokyoTM; 

Tokyo 24/7 (=test) and TokyoTM train/val are all geographically disjoint. Further details on the splits are given in the appendix [2].

Evaluation metric. We follow the standard place recognition evaluation procedure [5, 24, 64, 79, 80]. The query image is deemed correctly localized if at least one of the top N retrieved database images is within d =25 meters from the ground truth position of the query. The percentage of correctly recognized queries (Recall) is then plotted for different values ofN. For Tokyo 24/7 we follow [79] and perform spatial non-maximal suppression on ranked database images before evaluation. 

Implementation details. We use two base architectures which are extended with Max pooling (fmax) and our NetVLAD (fVLAD) layers: AlexNet [37] and VGG-16 [72]; both are cropped at the last convolutional layer (conv5), before ReLU. For NetVLAD we use K =64 resulting in 16k and 32k-D image representations for the two base architectures, respectively. The initialization procedure, parameters used for training, procedure for sampling training tuples and other implementation details are given in the appendix [2]. All training and evaluation code, as well as our trained networks, is online at [1].

我们报告两个公开数据集的结果。

Pittsburgh（Pitts250k）[80]包含从Google街景视图下载的250k数据库图像和从街景视图生成的24k测试查询，但是在相隔数年的不同时间拍摄。我们将此数据集划分为三个大致相等的部分用于训练，验证和测试，每个部分包含大约83k数据库图像和8k查询，其中划分在地理上完成以确保集合包含独立图像。为了便于更快的训练，对于一些实验，使用较小的子集（Pitts30k），在每个列车/ val（idation）/测试集中包含10k数据库图像，这些图像在地理上也是不相交的。

东京24/7 [79]包含76k数据库图像和使用手机摄像头拍摄的315个查询图像。这是一个非常具有挑战性的数据集，其中查询是在白天，日落和夜晚进行的，而数据库图像仅在白天拍摄，因为它们来自Google Street View，如上所述。为了形成火车/瓦尔组，我们**使用Time Machine功能**收集了东京的其他Google街景全景图，并将此组命名为TokyoTM;

东京24/7（=测试）和TokyoTM火车/瓦尔在地理上都是不相交的。关于分裂的更多细节在附录[2]中给出。

评估指标。我们遵循标准的地点识别评估程序[5,24,64,79,80]。如果前N个检索到的数据库图像中的至少一个在距离查询的地面实况位置d = 25米内，则认为查询图像被正确地定位。然后针对N的不同值绘制正确识别的查询（Recall）的百分比。对于东京24/7，我们遵循[79]并在评估之前对排名的数据库图像执行空间非最大抑制。

实施细节。我们使用两个基本体系结构，这些体系结构使用Max pooling（fmax）和我们的NetVLAD（fVLAD）层进行扩展：AlexNet [37]和VGG-16 [72];在ReLU之前，两者都在最后一个卷积层（conv5）被裁剪。对于NetVLAD，我们使用K = 64，分别为两个基础架构产生16k和32k-D图像表示。初始化过程，用于训练的参数，训练元组的采样程序和其他实现细节在附录[2]中给出。所有培训和评估代码以及我们训练有素的网络都在线[1]。

5.2. Results and discussion 

Baselines and state-of-the-art. To assess benefits of our approach we compare our representations trained for place recognition against “off-the-shelf” networks pretrained on other tasks. Namely, given a base network cropped at conv5, the baselines either use Max pooling (fmax), or aggregate the descriptors into VLAD (fVLAD), but perform no further task-specific training. The three base networks are: AlexNet [37], VGG-16 [72], both are pretrained for ImageNet classification [19], and Places205 [89], reusing the same architecture as AlexNet but pretrained for scene classification [89]. Pretrained networks have been recently used as off-the-shelf dense descriptor extractors for instance retrieval [7, 8, 23, 60, 61] and the untrained fmax network corresponds to the method of [7, 61]

Furthermore we compare our CNN representations trained for place recognition against the state-of-the-art local feature based compact descriptor, which consists of VLAD pooling [29] with intra-normalization [4] on top of densely extracted RootSIFTs [3, 43]. The descriptor is optionally reduced to 4096 dimensions using PCA (learnt on the training set) combined with whitening and L2-normalization [25]; this setup together with view synthesis yields the state-of-the-art results on the challenging Tokyo 24/7 dataset (c.f.[79]). 

基线和最先进的。为了评估我们的方法的好处，我们将我们为地点识别培训的表示与其他任务上预先训练的“现成”网络进行比较。即，给定在conv5处裁剪的基础网络，基线要么使用最大池化（fmax），要么将描述符聚合到VLAD（fVLAD）中，而是不执行任何进一步的任务特定训练。三个基础网络是：AlexNet [37]，VGG-16 [72]，两者都是针对ImageNet分类[19]和Places205 [89]预先训练的，重复使用与AlexNet相同的体系结构，但是预先训练用于场景分类[89]。最近，预训练网络被用作现成的密集描述符提取器，例如检索[7,8,23,60,61]，未经训练的fmax网络对应于[7,61]的方法。

此外，我们将经过地点识别训练的CNN表示与最先进的基于局部特征的紧凑描述符进行比较，该描述符包括VLAD汇集[29]，在密集提取的RootSIFT之上进行内部归一化[4][3,43] ]。使用PCA（在训练集上学习）结合白化和L2标准化[25]，描述符可选地减少到4096维;这种设置与视图合成一起产生了具有挑战性的东京24/7数据集（c.f. [79]）的最新结果。

In the following we discuss figure 5, which compares place recognition performance of our method to the baselines outlined above on the **Pittsburgh and Tokyo 24/7 benchmarks.**

Dimensionality reduction. We follow the standard stateof-the-art procedure to perform dimensionality reduction of VLAD, as described earlier, i.e. the reduction into 4096-D is performed using PCA with whitening followed by L2normalization [25, 79]. Figure 5 shows that the lower divector (-o-). mensional fVLAD (-∗-) performs similarly to the full size 

Benefits of end-to-end training for place recognition. Representations trained on the end-task of place recognition consistently outperform by a large margin off-theshelf CNNs on both benchmarks. For example, on the Pitts250k-test our trained AlexNet with (trained) NetVLAD aggregation layer achieves recall@1 of 81.0% compared to only 55.0% obtained by off-the-shelf AlexNet with standard VLAD aggregation, i.e. a relative improvement in recall of 47%. Similar improvements can be observed on all three datasets. This confirms two important premises of this work: (i) our approach can learn rich yet compact image representations for place recognition, and (ii) the popular idea of using pretrained networks “off-the-shelf” [7, 8, 23, 60, 61] is sub-optimal as the networks trained for object or scene classification are not necessary suitable for the end-task of place recognition. We believe this could be attributed to the fact that “off-the-shelf ” conv5 activations are not trained to be comparable using Euclidean distance. 

在下文中，我们将讨论图5，该图比较了我们的方法的位置识别性能与匹兹堡和东京24/7基准测试中的上述基线。

维度降低。我们遵循标准的现有技术程序来执行VLAD的降维，如前所述，即使用具有白化的PCA然后进行L2标准化[25,79]进行4096-D的降低。图5显示了下部潜水器（-o-）。男性fVLAD（ - * - ）的表现与全尺寸相似

地点识别的端到端培训的好处。在地理位置识别的最终任务方面受过培训的代表人员在这两个基准测试中始终优于大规模的现有CNN。例如，在Pitts250k测试中，我们训练有素的AlexNet与（受过训练的）NetVLAD聚合层实现了召回@ 1的81.0％，而现有的AlexNet与标准的VLAD聚合仅获得55.0％，即召回的相对改善47％。在所有三个数据集上都可以观察到类似的改进。这证实了这项工作的两个重要前提：（i）我们的方法可以学习用于地点识别的丰富而紧凑的图像表示，以及（ii）使用预先训练的网络“现成”的流行思想[7,8,23，因为针对对象或场景分类训练的网络不一定适合于场所识别的最终任务，所以60,61]是次优的。我们认为这可能是因为“现成的”conv5激活没有经过训练，无法使用欧几里德距离进行比较。

Comparison with state-of-the-art. Figure 5 also shows that our trained fVLAD representation with whitening based on VGG-16 (magenta -∗-) convincingly outperforms RootSIFT+VLAD+whitening, as well as the method of Torii et al.[79], and therefore sets the state-of-the-art for compact descriptors on all benchmarks. Note that these are strong baselines that outperform most off-the-shelf CNN descriptors on the place recognition task

VLAD versus Max. By comparing fVLAD (-o-) methods with their corresponding fmax (-x-) counterparts it is clear that VLAD pooling is much better than Max pooling for both off-the-shelf and trained representations. NetVLAD performance decreases gracefully with dimensionality: 128-D NetVLAD performs similarly to 512-D Max (42.9% vs 38.4% recall@1 on Tokyo 24/7), resulting in four times more compact representation for the same performance. Furthermore, NetVLAD+whitening outperforms Max pooling convincingly when reduced to the same dimensionality (60%). See the appendix [2] for more details. 

Which layers should be trained? In Table 1 we study the benefits of training different layers for the end-task of place recognition. The largest improvements are thanks to training the NetVLAD layer, but training other layers results in further improvements, with some overfitting occurring below conv2.

与最先进的技术进行比较。图5还显示我们训练的基于VGG-16（品红色 - * - ）的美白fVLAD表示令人信服地优于RootSIFT + VLAD +美白，以及Torii等[79]的方法，因此设置了状态 - 所有基准测试中的紧凑描述符的最新技术。请注意，这些是强大的基线，在地点识别任务上胜过大多数现成的CNN描述符

VLAD与Max。通过将fVLAD（-o-）方法与其对应的fmax（-x-）对应物进行比较，显然对于现成和训练的表示，VLAD汇集比Max汇集好得多。 NetVLAD性能随着维度而优雅地降低：128-D NetVLAD的表现与512-D Max相似（42.9％对比东京24/7上的38.4％召回@ 1），导致相同性能的紧凑表示的四倍。此外，NetVLAD +白化在降低到相同维度（60％）时令人信服地优于Max pool。有关详细信息，请参阅附录[2]。

应该培训哪些层？在表1中，我们研究了培训不同层次对于场所识别最终任务的好处。最大的改进是通过训练NetVLAD层，但是训练其他层导致进一步改进，在conv2下面发生一些过度拟合。

Importance of Time Machine training. Here we examine whether the network can be trained without the Time Machine (TM) data. In detail, we have modified the training query set for Pitts30k-train to be sampled from the same set as the training database images, i.e. the tuples of query and database images used in training were captured at the same time. Recall@1 with fmax on Pitts30k-val for the offthe-shelf AlexNet is 33.5%, and training without TM improves this to 38.7%. However, training with TM obtains 68.5% showing that Time Machine data is crucial for good place recognition accuracy as without it the network does not generalize well. The network learns, for example, that recognizing cars is important for place recognition, as the same parked cars appear in all images of a place.

Time Machine培训的重要性。 在这里，我们检查是否可以在没有Time Machine（TM）数据的情况下训练网络。 详细地说，我们已经修改了Pitts30k-train的训练查询集，该训练查询集是从与训练数据库图像相同的集合中采样的，即同时捕获训练中使用的查询和数据库图像的元组。 对于现成的AlexNet，召回@ 1和fts on Pitts30k-val为33.5％，没有TM的训练将此提高到38.7％。 然而，使用TM的培训得到68.5％，表明Time Machine数据对于良好的场所识别准确性至关重要，因为没有它，网络不能很好地概括。 例如，网络学习识别汽车对于地点识别很重要，因为相同的停放汽车出现在一个地方的所有图像中。

5.3. Qualitative evaluation 

To visualize what is being learnt by our place recognition architectures, we adapt the method of Zeiler and Fergus [87] for examining occlusion sensitivity of classification networks. It can be seen in figure 6 that off-the-shelf AlexNet (pretrained on ImageNet) focuses very much on categories it has been trained to recognize (e.g. cars) and certain shapes, such as circular blobs useful for distinguishing 12 different ball types in the ImageNet categories. The Place205 network is fairly unresponsive to all occlusions as it does not aim to recognize specific places but scene-level categories, so even if an important part of the image is occluded, such as a characteristic part of a building fac¸ade, it still provides a similar output feature which corresponds to an uninformative “a building fac¸ade” image descriptor. In contrast to these two, our network trained for specific place recognition automatically learns to ignore confusing features, such as cars and people, which are not discriminative for specific locations, and instead focuses on describing building fac¸ades and skylines. More qualitative examples are provided in the appendix [2].

为了可视化我们的场所识别架构所学到的东西，我们采用**Zeiler和Fergus [87]的方法来检验分类网络的遮挡敏感性**。从图6中可以看出，现成的AlexNet（在ImageNet上预训练）非常关注它已被训练识别的类别（例如汽车）和某些形状，例如用于区分12种不同球类型的圆形blob。 ImageNet类别。 Place205网络对所有遮挡都没有反应，因为它的目的不是识别特定场所而是场景级别，因此即使图像的一个重要部分被遮挡，例如建筑物设施的特征部分，它仍然提供类似的输出特征，其对应于无信息的“建筑物设施”图像描述符。与这两者相比，我们为特定地点识别而训练的网络自动学会忽略混淆的特征，例如汽车和人，这些特征不是针对特定位置的歧视，而是专注于描述建筑物外观和天际线。附录中提供了更多定性的例子[2]。

5.4. Image retrieval 

We use our best performing network (VGG-16, fVLAD with whitening down to 256-D) trained completely on Pittsburgh, to extract image representations for standard object and image retrieval benchmarks. Our representation sets the state-of-the-art for compact image representations (256-D) by a large margin on all three datasets, obtaining an mAP of 63.5%, 73.5% and 79.9% on Oxford 5k [53], Paris 6k [54], Holidays [26], respectively; for example, this is a +20% relative improvement on Oxford 5k. The appendix [2] contains more detailed results.

我们使用在匹兹堡完全训练的最佳表现网络（VGG-16，fVLAD，美白低至256-D），提取标准物体和图像检索基准的图像表示。 我们的表示在所有三个数据集上以较大的幅度设置了紧凑图像表示（256-D）的最新技术，在Oxford 5k [53]，巴黎6k上获得63.5％，73.5％和79.9％的mAP [54]，假期[26]，分别; 例如，牛津5k的相对改善率为+ 20％。 附录[2]包含更详细的结果。

#### VI. Conclusions

We have designed a new convolutional neural network architecture that is trained for place recognition in an endto-end manner from weakly supervised Street View Time Machine data. Our trained representation significantly outperforms off-the-shelf CNN models and significantly improves over the state-of-the-art on the challenging 24/7 Tokyo dataset, as well as on the Oxford and Paris image retrieval benchmarks. The two main components of our architecture – (i) the NetVLAD pooling layer and (ii) weakly supervised ranking loss – are generic CNN building blocks applicable beyond the place recognition task. The NetVLAD layer offers a powerful pooling mechanism with learnable parameters that can be easily plugged into any other CNN architecture. The weakly supervised ranking loss opens up the possibility of end-to-end learning for other ranking tasks where large amounts of weakly labelled data are available, for example, images described with natural language

我们设计了一种新的卷积神经网络架构，该架构通过弱监督的街景时间机器数据以端到端的方式进行地点识别训练。 我们训练有素的代表性显着优于现成的CNN模型，并在具有挑战性的24/7东京数据集以及牛津和巴黎图像的基准测试基准上对现有技术进行了显着改进。。 我们架构的两个主要组成部分 （i）NetVLAD汇集层和（ii）弱控制的排名损失 是超出场所识别任务的通用CNN构建块。 NetVLAD层提供了强大的池化机制，其中包含可学习的参数，可以轻松插入任何其他CNN架构。 弱监督排名损失为其他排名任务开辟了端到端学习的可能性，其中大量弱标签数据可用，例如，用自然语言描述的图像





