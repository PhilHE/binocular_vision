### mobilenet + graph segment + loss function

> 细节:newFV?
>
> 添加空间相对关系
>
> segnet

- Graph Based Image Segmentation

主页：http://cs.brown.edu/people/pfelzens/segment/(c)

http://cs.brown.edu/people/pfelzens/engn2520/

http://cs.brown.edu/people/pfelzens/engn1610/

将seg后的区块作为输入



解说：

- [图像分割—基于图的图像分割（Graph-Based Image Segmentation）](https://blog.csdn.net/ttransposition/article/details/38024557)
- [图像分割—基于图的图像分割(OpenCV源码注解)](https://blog.csdn.net/ttransposition/article/details/38024605)
- [图像分割之（一）概述](https://blog.csdn.net/zouxy09/article/details/8532106)
- [Efficient Graph-Based Image Segmentation解读](https://blog.csdn.net/mao_kun/article/details/50576036)
- [综述----图像分割综述](https://blog.csdn.net/Julialove102123/article/details/80493066)

**代码**

c

https://github.com/suryanshkumar/GraphSegmentation

py

https://github.com/salaee/pegbis

https://github.com/davidstutz/graph-based-image-segmentation

https://github.com/valhongli/PF_Segmentation

matlab

https://www.mathworks.com/matlabcentral/fileexchange/25866-efficient-graph-based-image-segmentation

编译不成功

https://github.com/jamesyin96/Graph_based_Image_Segmentation

效果不好

https://github.com/kuangliu/graph_seg

效果不好

https://github.com/cvjena/Felzenszwalb-Segmentation

混合编译，但似乎用vs不成功





- mobilenet

https://github.com/search?l=C%2B%2B&q=mobilenet&type=Repositories

https://github.com/KeyKy/mobilenet-mxnet

- 其他loss举例

**Enhancing Convolutional Neural Networks for Face Recognition with Occlusion Maps and Batch Triplet Loss**

Triple loss https://arxiv.org/abs/1511.07247

Google街景数据的特点是标记有GPS位置信息，但是在同一位置上拍摄的照片由于角度不同可能会得到完全不相关的图像，因此需要一些手段得到同一地点下与查询图像最为接近的图像：

其中是一组可能与查询图像q相关的数据集，通过上式可得到最可能与查询图像匹配的图像。之后再借助与查询图像不相关的图像可以组成用于训练triple loss的数据。据此定义三元组损失函数：

其中l是一个hinge loss l(x)=max(x,0), m是一个常数giving the margin.

原文：https://blog.csdn.net/wangxinsheng0901/article/details/81948440 





### 概念

- 端对端

[关于神经网络，端对端](https://blog.csdn.net/qq_24203757/article/details/79666574)

**端到端指的是输入是原始数据，输出是最后的结果**，非端到端的输入端不是直接的原始数据，而是在原始数据中提取的特征，这一点在图像问题上尤为突出，因为图像像素数太多，数据维度高，会产生维度灾难，所以原来一个思路是手工提取图像的一些关键特征，这实际就是就一个降维的过程。
那么问题来了，特征怎么提？

特征提取的好坏异常关键，甚至比学习算法还重要，举个例子，对一系列人的数据分类，分类结果是性别，如果你提取的特征是头发的颜色，无论分类算法如何，分类效果都不会好，如果你提取的特征是头发的长短，这个特征就会好很多，但是还是会有错误，如果你提取了一个超强特征，比如染色体的数据，那你的分类基本就不会错了。
这就意味着，特征需要足够的经验去设计，这在数据量越来越大的情况下也越来越困难。
于是就出现了端到端网络，特征可以自己去学习，所以特征提取这一步也就融入到算法当中，不需要人来干预了。